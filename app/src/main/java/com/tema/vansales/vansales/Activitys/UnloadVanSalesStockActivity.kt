package com.tema.vansales.vansales.Activitys

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.Adapters.LoadStockAdapter
import com.tema.vansales.vansales.Adapters.UnLoadStockAdapter
import com.tema.vansales.vansales.Model.LoadStockDO
import com.tema.vansales.vansales.R
import java.util.ArrayList

//
class UnloadVanSalesStockActivity : BaseActivity() {
    lateinit var recycleview :RecyclerView
    lateinit var loadStockAdapter: UnLoadStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.unload_van_sale_stock_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.unload_vansale_stock)
        recycleview = findViewById(R.id.recycleview) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)

        recycleview.setLayoutManager(linearLayoutManager)
        loadStockAdapter = UnLoadStockAdapter(this@UnloadVanSalesStockActivity,  null)
        recycleview.setAdapter(loadStockAdapter)
    }
}