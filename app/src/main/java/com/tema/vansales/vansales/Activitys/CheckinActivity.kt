package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RelativeLayout
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import com.tema.vansales.vansales.Adapters.RouteIdsAdapter
import com.tema.vansales.vansales.Adapters.RouteIdsEMAdapter

import com.tema.vansales.vansales.Model.DriverIdMainDO
import com.tema.vansales.vansales.Model.VehicleCheckMainDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.DriverIdRequest
import com.tema.vansales.vansales.Requests.EmptyDriverIdRequest
import com.tema.vansales.vansales.Requests.VehicleCheckinRequest
import com.tema.vansales.vansales.utils.PreferenceUtils

import java.util.ArrayList
import java.util.Calendar


class CheckinActivity : BaseActivity() {
    lateinit var country: Array<String>
    lateinit var vehicleCheckinRequest: VehicleCheckinRequest
    lateinit var username: String
    lateinit var routeId: String
    lateinit var carrierId: String
    internal var item: String? = null
    internal var safety: String? = null
    internal var document: String? = null
    lateinit var name: TextView
    lateinit var carrier: TextView
    lateinit var id: TextView
    private var llAboutUs: RelativeLayout? = null
    lateinit var spinner: Spinner
    internal var button: Button? = null
    internal var selectedId: Int = 0
    lateinit var genderradioButton: RadioButton
    lateinit var radioGroup: RadioGroup
    lateinit var cb1: CheckBox
    lateinit var cb2: CheckBox
    lateinit var cb3: CheckBox
    internal var cb1Id: Int = 0
    internal var cb2Id: Int = 0
    lateinit var driverIdMainDo: DriverIdMainDO
    lateinit var c: Calendar
    internal var mYear: Int = 0
    internal var mMonth: Int = 0
    internal var mDay: Int = 0
    lateinit var tvSelection: TextView
    lateinit var tvNotes: TextView

    lateinit var dialog: Dialog
    override fun initialize() {

        llAboutUs = layoutInflater.inflate(R.layout.check_in_screen, null) as RelativeLayout
        llBody.addView(llAboutUs, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }

        //  disableMenuWithBackButton();
        preferenceUtils = PreferenceUtils(this@CheckinActivity)
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "")
        routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        carrierId = preferenceUtils.getStringFromPreference(PreferenceUtils.CARRIER_ID, "");

        tvSelection = findViewById(R.id.tvSelection) as TextView
        var rlSelection = findViewById(R.id.vehicle_id_RL) as RelativeLayout
        tvNotes = findViewById(R.id.tvNotes) as TextView

        if (routeId.length > 0) {

            tvSelection.setText("" + routeId)
        } else {
            tvSelection.setText("Select Route Id")

        }
        preferenceUtils = PreferenceUtils(this@CheckinActivity)
        routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        carrierId = preferenceUtils.getStringFromPreference(PreferenceUtils.CARRIER_ID, "");

        val siteListRequest = DriverIdRequest(routeId, this@CheckinActivity)
        showLoader()
        siteListRequest.setOnResultListener { isError, driverIdMainDO ->
            hideLoader()
            driverIdMainDo = driverIdMainDO
            if (isError) {
                Toast.makeText(this@CheckinActivity, R.string.error_site_list, Toast.LENGTH_SHORT).show()
            } else {
                tvNotes.setText("" + driverIdMainDO.notes)

            }
        }
        // routeId = driverIdMainDo.driverDOS[0].vehicleRouteId
        //carrierId = driverIdMainDo.driverDOS[0].vehicleCarrier

        siteListRequest.execute()



        rlSelection.setOnClickListener {
            dialog = Dialog(this, R.style.NewDialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.invoice_list_dialog)

            val window = dialog.getWindow()
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

            var recyclerView = dialog.findViewById(R.id.recycleview) as RecyclerView
            var recycleviewEM = dialog.findViewById(R.id.recycleviewEM) as RecyclerView

            recycleviewEM.setLayoutManager(LinearLayoutManager(this));
            recyclerView.setLayoutManager(LinearLayoutManager(this));
            if (driverIdMainDo != null) {

                var siteAdapter = RouteIdsAdapter(this@CheckinActivity, driverIdMainDo.driverDOS)
                recyclerView.setAdapter(siteAdapter)

                var em = RouteIdsEMAdapter(this@CheckinActivity, driverIdMainDo.driverEmptyDOS)
                recycleviewEM.setAdapter(em)
            }else{
                showAlert("No Data Found")
            }
//
//
//            val empty = EmptyDriverIdRequest(routeId,this@CheckinActivity)
//            showLoader()
//            empty.setOnResultListener { isError, driverIdMainDO ->
//                hideLoader()
//                driverIdMainDo = driverIdMainDO
//                if (isError) {
//                    Toast.makeText(this@CheckinActivity, R.string.error_site_list, Toast.LENGTH_SHORT).show()
//                } else {
//
//                    var siteAdapter = RouteIdsAdapter(this@CheckinActivity, driverIdMainDo.driverDOS)
//                    recyclerView.setAdapter(siteAdapter)
//
//                   var em = RouteIdsEMAdapter(this@CheckinActivity, driverIdMainDo.driverDOS.get(0).driverEmptyDOS)
//                   recyclerView.setAdapter(em)
//                }
//            }
//            routeId = driverIdMainDo.driverDOS[0].vehicleRouteId
//            carrierId = driverIdMainDo.driverDOS[0].vehicleCarrier
//
//            empty.execute()

            dialog.show()


        }


//
//
//        val driverIdRequest = DriverIdRequest(username, this@CheckinActivity)
//        //  showLoader();
//        driverIdRequest.setOnResultListener { isError, driverIdMainDO ->
//            hideLoader()
//            driverIdMainDo = driverIdMainDO
//            if (isError) {
//                Toast.makeText(this@CheckinActivity, R.string.error_checkin, Toast.LENGTH_SHORT).show()
//            } else {
//                if (driverIdMainDo.driverDOS.size > 0) {
//                    val contacts = ArrayList<Contact>()
//
//                    for (i in driverIdMainDo.driverDOS.indices) {
//                        contacts.add(Contact(driverIdMainDo.driverDOS[i].vehicleRouteId))
//
//                    }
//
//
//                    val adapter = ArrayAdapter(applicationContext, R.layout.spinner_item, contacts)
//                    adapter.setDropDownViewResource(R.layout.spinner_item)
//                    spinner.adapter = adapter
//
//                    for (i in driverIdMainDO.driverDOS.indices) {
//                        country = arrayOf(driverIdMainDO.driverDOS[i].vehicleRouteId)
//                        name.text = driverIdMainDO.driverName
//                        carrier.text = "" + driverIdMainDO.driverDOS[i].vehicleCarrier
//                        id.text = "" + driverIdMainDO.driverDOS[i].vehicleCode
//                    }
////                    if (driverIdMainDo.driverEmptyDOS.get(0).emptyVehicleCode.length>0) {
////
////                    }
//                } else {
//                    showAlert("No Routing Id's Found")
//                }
//
//
//                //ArrayAdapter adapter = new ArrayAdapter(CheckinActivity.this, android.R.layout.simple_spinner_item, country);
//                //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                //spinner.setAdapter(adapter);
//
//            }
//        }
//        driverIdRequest.execute()


    }

//    fun onclickbuttonMethod(v: View) {
//        selectedId = radioGroup.checkedRadioButtonId
//        genderradioButton = findViewById<View>(selectedId) as RadioButton
//        if (selectedId == -1) {
//            // Toast.makeText(CheckinActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
//        } else {
//            if (genderradioButton.text == R.string.cylinder) {
//                selectedId = 1
//            } else if (genderradioButton.text == R.string.lpg) {
//                selectedId = 2
//            } else {
//                selectedId = 3
//            }
//            // Toast.makeText(CheckinActivity.this, genderradioButton.getText(), Toast.LENGTH_SHORT).show();
//        }
//
//    }

    private inner class Contact(private val contact_name: String) {
        override fun toString(): String {
            return contact_name
        }
    }

    override fun initializeControls() {

        tvScreenTitle.setText(R.string.check_in)

        val btnCheckIn = findViewById<Button>(R.id.btnCheckIn)
        val btnVehicleInspection = findViewById<Button>(R.id.btnVehicleInspection)
        val btnGateInspection = findViewById<Button>(R.id.btnGateInspection)
        btnGateInspection.setOnClickListener {
            val intent = Intent(this@CheckinActivity, GateInspectonActivity::class.java)
            startActivity(intent)
        }
        btnVehicleInspection.setOnClickListener {
            val intent = Intent(this@CheckinActivity, VehicleInspectionActivity::class.java)
            startActivity(intent)
        }
        spinner = findViewById<View>(R.id.spinner) as Spinner
        name = findViewById(R.id.name)
        carrier = findViewById(R.id.carrier)
        id = findViewById(R.id.id)
        radioGroup = findViewById<View>(R.id.radioGroup) as RadioGroup
        cb1 = findViewById<View>(R.id.cb1) as CheckBox
        cb2 = findViewById<View>(R.id.cb2) as CheckBox
        cb3 = findViewById<View>(R.id.cb3) as CheckBox
        cb1.isChecked = true
        cb2.isChecked = true
        cb3.isChecked = true

        btnCheckIn.setOnClickListener { v ->
            //onclickbuttonMethod(v)
            if (cb1.isChecked) {
                cb1Id = 2
            } else {
                cb1Id = 1
            }
            if (cb2.isChecked) {
                cb2Id = 2

            } else {
                cb2Id = 1
            }
            if (cb3.isChecked) {
                // cb3Id =2;

            }
            routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
            carrierId = preferenceUtils.getStringFromPreference(PreferenceUtils.CARRIER_ID, "");

            if (driverIdMainDo.driverDOS.size > 0 && routeId.length > 0 && carrierId.length > 0) {

                vehicleCheckinRequest = VehicleCheckinRequest(
                        driverIdMainDo.driverName, routeId, carrierId, "" + 1, "" + cb1Id, "" + cb2Id, this@CheckinActivity)
                vehicleCheckinRequest.setOnResultListener { isError, vehicleCheckMainDO ->
                    hideLoader()

                    if (isError) {
                        Toast.makeText(this@CheckinActivity, R.string.error_checkin, Toast.LENGTH_SHORT).show()
                    } else {
                        preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, routeId)
                        c = Calendar.getInstance()
                        mYear = c.get(Calendar.YEAR)
                        mMonth = c.get(Calendar.MONTH)
                        mDay = c.get(Calendar.DAY_OF_MONTH)
                        preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, StringBuilder().append(mMonth + 1).append("-").append(mDay).append("-").append(mYear).append("   " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)).toString())

                        val intent = Intent(this@CheckinActivity, DashBoardActivity::class.java)
                        startActivity(intent)

                    }
                }
                vehicleCheckinRequest.execute()

            }
        }
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //   Toast.makeText(getApplicationContext(),""+position , Toast.LENGTH_LONG).show();
                //                spinner.setSelection(0);
                //                if (position != 0) {
                //                    showAlert("Please Complete first Routing");
                //                }


            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


    }
}
