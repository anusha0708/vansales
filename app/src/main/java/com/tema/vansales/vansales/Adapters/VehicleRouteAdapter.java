package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tema.vansales.vansales.Activitys.ActiveDeliveryActivity;
import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.Activitys.CurrentMapActivity;
import com.tema.vansales.vansales.Activitys.PODActivity;
import com.tema.vansales.vansales.Activitys.ProductDetailsActivity;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.Model.ProductDO;
import com.tema.vansales.vansales.Model.RouteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.Calendar;
import java.util.List;

public class VehicleRouteAdapter extends RecyclerView.Adapter<VehicleRouteAdapter.MyViewHolder> implements LocationListener {

    private List<PickUpDo> productDOS;
    private Context context;
    Double lat, lng;
    double dist;

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        ((BaseActivity) context).preferenceUtils = new PreferenceUtils(context);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvCount, tvTop, tvAddress, tvTime, tvDistance;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            tvDistance = (TextView) view.findViewById(R.id.tvDistance);

            tvTop = (TextView) view.findViewById(R.id.tvTop);
            tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            tvTime = (TextView) view.findViewById(R.id.tvTime);


        }
    }


    public VehicleRouteAdapter(Context context, List<PickUpDo> productDOS) {
        this.context = context;
        this.productDOS = productDOS;
    }

    @SuppressLint("MissingPermission")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicle_data, parent, false);
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        lat = location.getLongitude();
        lng = location.getLatitude();
        double lat2 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0));
        double lng3 = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0));



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final PickUpDo productDO = productDOS.get(position);
        holder.tvName.setText(productDO.description);
        holder.tvAddress.setText(productDO.customer);
        distance(lat, lng, productDO.lattitude, productDO.longitude);
        double value = Double.parseDouble(String.format("%.2f",dist));
        holder.tvDistance.setText(""+value+"km");

        // Double distance = mylocation.distanceTo(dest_location);//in meters
        String arrivalTime = productDO.arrivalTime.substring(Math.max(productDO.arrivalTime.length() - 2, 0));
        String departureTime = productDO.departureTime.substring(Math.max(productDO.departureTime.length() - 2, 0));
        System.out.println(arrivalTime);
        System.out.println( departureTime);
        String arrivalTime2 = productDO.arrivalTime.substring(0,2);
        String departureTime2 = productDO.departureTime.substring(0,2);

        holder.tvTime.setText("ETA : " + arrivalTime2+":"+arrivalTime + "  ETD : " +  departureTime2+":"+departureTime);
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        preferenceUtils.saveString(PreferenceUtils.SHIPMENT_ID, String.valueOf(productDOS.get(0).shipmentNumber));
        preferenceUtils.saveString(PreferenceUtils.CUSTOMER, String.valueOf(productDOS.get(0).customer));
        preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDOS.get(0).lattitude);
        preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDOS.get(0).longitude);
        preferenceUtils.saveString(PreferenceUtils.SHIPMENT_ID, String.valueOf(productDOS.get(0).shipmentNumber));
        preferenceUtils.saveString(PreferenceUtils.SHIPMENT_AT, productDOS.get(0).arrivalDate + " " + productDOS.get(0).arrivalTime);
        preferenceUtils.saveString(PreferenceUtils.PHONE, String.valueOf(productDOS.get(0).sequenceId));
        preferenceUtils.saveString(PreferenceUtils.EMAIL, String.valueOf(productDOS.get(0).netWeight));
        preferenceUtils.saveString(PreferenceUtils.SHIPMENT_DT, productDOS.get(0).departureDate + " " + productDOS.get(0).departureTime);
        int pos = position + 1;
        if (pos == 1) {
            holder.tvTop.setVisibility(View.GONE);
        } else
            holder.tvTop.setVisibility(View.VISIBLE);

        holder.tvCount.setText(+pos + "");
        pos++;
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);

                preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDO.lattitude);
                preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDO.longitude);

                Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                intent.putExtra("", productDO.arrivalTime + productDO.arrivalDate);
//                intent.putExtra("Code", productDO.vehicleRoutingId);
                context.startActivity(intent);


            }
        });

        double lat = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE2, 0.0));
        double lng = Double.valueOf(((BaseActivity) context).preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE2, 0.0));

    }

    @Override
    public int getItemCount() {
        return productDOS.size();
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
