package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import com.tema.vansales.vansales.Adapters.CreateInvoiceAdapter
import com.tema.vansales.vansales.Adapters.SiteAdapter
import com.tema.vansales.vansales.Model.CreateInvoiceDO
import com.tema.vansales.vansales.Model.LoadStockMainDO
import com.tema.vansales.vansales.Model.PickUpDo
import com.tema.vansales.vansales.Model.SiteDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.CreateInvoiceRequest
import com.tema.vansales.vansales.Requests.CreatePaymentRequest
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest
import com.tema.vansales.vansales.Requests.SiteListRequest
import com.tema.vansales.vansales.utils.LogUtils
import com.tema.vansales.vansales.utils.PreferenceUtils


class CreateInvoiceActivity : BaseActivity() {
    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var pickDos: ArrayList<PickUpDo>
    lateinit var createInvoiceDO: CreateInvoiceDO

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvSelection = findViewById(R.id.tvSelection) as TextView
        tvSelection.setText("Select Site")
        preferenceUtils = PreferenceUtils(this)
        tvSelection.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, ""))
        var data = tvSelection.text.toString()
        var  btnInvoice           = findViewById(R.id.btnInvoice) as Button
        btnInvoice.setOnClickListener {


            val siteListRequest = CreateInvoiceRequest(data,this@CreateInvoiceActivity)
            siteListRequest.setOnResultListener { isError, createPaymentDO ->
                hideLoader()
                if(createPaymentDO!=null){


                createInvoiceDO =createPaymentDO
                if (isError) {
                    Toast.makeText(this@CreateInvoiceActivity, R.string.error_site_list, Toast.LENGTH_SHORT).show()
                } else {
                    preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, createInvoiceDO.salesInvoiceNumber)

                    Toast.makeText(this@CreateInvoiceActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)
                }
                }
            }

            siteListRequest.execute()
        }

        tvSelection.setOnClickListener {
            dialog = Dialog(this, R.style.NewDialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.invoice_list_dialog)
            val window = dialog.getWindow()
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

            var recyclerView = dialog.findViewById(R.id.recycleview) as RecyclerView
            recyclerView.setLayoutManager(LinearLayoutManager(this));
            preferenceUtils = PreferenceUtils(this@CreateInvoiceActivity)
            var id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");

            val siteListRequest = DisplayPickupListRequest(this@CreateInvoiceActivity, id)
            showLoader()
            siteListRequest.setOnResultListener { isError, siteDoS ->
                hideLoader()
                pickDos = siteDoS
                if (isError) {
                    Toast.makeText(this@CreateInvoiceActivity, R.string.error_site_list, Toast.LENGTH_SHORT).show()
                } else {

                    var siteAdapter = CreateInvoiceAdapter(this@CreateInvoiceActivity, pickDos)
                    recyclerView.setAdapter(siteAdapter)

                }
            }

            siteListRequest.execute()

            dialog.show()


        }

    }


}