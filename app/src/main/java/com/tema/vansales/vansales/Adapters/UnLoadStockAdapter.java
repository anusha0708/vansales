package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tema.vansales.vansales.Model.LoadStockDO;
import com.tema.vansales.vansales.Model.UnLoadStockDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class UnLoadStockAdapter extends RecyclerView.Adapter<UnLoadStockAdapter.MyViewHolder> /*implements Filterable*/ {
    int count=0;
    private ArrayList<UnLoadStockDO> unLoadStockDOS;
    private String imageURL;
    private Context context;
    private PreferenceUtils preferenceUtils;

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    listLunchDos = listLunchDos;
//                } else {
//                    ArrayList<LunchDo> filteredList = new ArrayList<>();
//                    for (LunchDo row : listLunchDos) {
//
//                        if (row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }
//                    }
//
//                    listLunchDos = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = listLunchDos;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                listLunchDos = (ArrayList<LunchDo>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription,tvNumber;
        public RelativeLayout rlRemove,rlAdd;
        public ImageView ivRemove,ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = (TextView) view.findViewById(R.id.tvName);
            tvDescription   = (TextView) view.findViewById(R.id.tvDescription);
//            rlRemove        = (RelativeLayout) view.findViewById(R.id.rlRemove);
//            rlAdd           = (RelativeLayout) view.findViewById(R.id.rlAdd);
            ivRemove        = (ImageView) view.findViewById(R.id.ivRemove);
            ivAdd           = (ImageView) view.findViewById(R.id.ivAdd);
            tvNumber        = (TextView) view.findViewById(R.id.tvNumber);
        }
    }


    public UnLoadStockAdapter(Context context, ArrayList<UnLoadStockDO> unLoadStockDOS) {
        this.context = context;
        this.unLoadStockDOS = unLoadStockDOS;

        preferenceUtils = new PreferenceUtils(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.unload_stock_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        final LoadStockDO loadStockDO =loadStockDOS.get(position);
//        holder.tvProductName.setText(loadStockDO.productName);
//        holder.tvDescription.setText(loadStockDO.productDescription);
//        holder.tvNumber.setText(""+loadStockDO.itemCount);


        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                unLoadStockDOS.get(position).itemCount = unLoadStockDOS.get(position).itemCount + 1;

                holder.tvNumber.setText(""+unLoadStockDOS.get(position).itemCount);


//                         count= count+1;
//                holder.tvNumber.setText(""+count);

            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(unLoadStockDOS.get(position).itemCount > 0){
                    unLoadStockDOS.get(position).itemCount = unLoadStockDOS.get(position).itemCount- 1;

                    holder.tvNumber.setText(""+unLoadStockDOS.get(position).itemCount);

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return 10;
    }

}
