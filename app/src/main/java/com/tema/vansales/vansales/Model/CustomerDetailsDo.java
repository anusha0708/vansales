package com.tema.vansales.vansales.Model;

import java.io.Serializable;

public class CustomerDetailsDo implements Serializable {


    public String addressLine = "";
    public String addressDescription = "";
    public String postalCode = "";
    public String city = "";
    public String telephone = "";
    public String mobile = "";

    public void addressLine(String text) {
        this.addressLine = text;
    }

    public void addressDescription(String text) {
        this.addressDescription = text;
    }

    public void telephone(String text) {
        this.telephone = text;
    }

    public void postalCode(String text) {
        this.postalCode = text;
    }

    public void mobile(String text) {
        this.mobile = text;
    }

    public void city(String text) {
        this.city = text;
    }

}
