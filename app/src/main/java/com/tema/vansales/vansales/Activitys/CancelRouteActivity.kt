package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.R

//
class CancelRouteActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_start_route, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.start_route)
        showAppCompatAlert("Directions", "Cancel navigation to Restaurent Mos Eisely?", "Yes", "No", "SUCCESS", false)


    }
    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
         finish()


        }
    }

    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {

            finish()
        }
    }

    }