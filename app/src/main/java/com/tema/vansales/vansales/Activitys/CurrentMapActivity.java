package com.tema.vansales.vansales.Activitys;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tema.vansales.vansales.Adapters.VehicleRouteAdapter;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.Model.PriceDetailsDO;
import com.tema.vansales.vansales.Model.RouteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest;
import com.tema.vansales.vansales.Requests.VehicleRouteListRequest;
import com.tema.vansales.vansales.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CurrentMapActivity extends BaseActivity implements OnMapReadyCallback {
    private LinearLayout llAboutUs;
    Double address;

    private static final String TAG = CurrentMapActivity.class.getSimpleName();

    private ImageButton backButton;
    String code;
    public GoogleMap googleMap;
    ArrayList<PickUpDo> pickUpDos;

    @Override
    public void initialize() {
        llAboutUs = (LinearLayout) getLayoutInflater().inflate(R.layout.map, null);
        llBody.addView(llAboutUs, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (getIntent().hasExtra("Code")) {
            code = getIntent().getExtras().getString("Code");
        }
        DisplayPickupListRequest driverListRequest = new DisplayPickupListRequest(CurrentMapActivity.this, code);

        driverListRequest.setOnResultListener(new DisplayPickupListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, ArrayList<PickUpDo> pods) {
                hideLoader();
                pickUpDos = pods;
                if (isError) {
                    Toast.makeText(CurrentMapActivity.this, R.string.error_map, Toast.LENGTH_SHORT).show();
                } else {
                    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

                    mapFragment.getMapAsync(CurrentMapActivity.this);
//                    VehicleRouteAdapter driverAdapter = new VehicleRouteAdapter(CurrentMapActivity.this, pods);
//                    recycleview.setLayoutManager(new LinearLayoutManager(CurrentMapActivity.this));
//
//                    recycleview.setAdapter(driverAdapter);

                }


            }
        });
        driverListRequest.execute();
    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText("MAP");
    }


    @Override
    public void onMapReady(GoogleMap googlemap) {
        googleMap = googlemap;
        googleMap.setMyLocationEnabled(true);
        if (googleMap == null) {
            Toast.makeText(getApplicationContext(), R.string.map_error, Toast.LENGTH_SHORT).show();
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true);

        }
        try {
            showLocation();

        } catch (Exception e) {
            Log.e(TAG, String.valueOf(e));
        }


    }

    private void drawMarker(LatLng point, String id) {
        LatLngBounds.Builder builder;
        CameraUpdate cu; // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();
        // Setting latitude and longitude for the marker
        markerOptions.position(point);
        markerOptions.position(point).title("shipmentID :").snippet(id);

        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);

//        googleMap.moveCamera(cu);
//        googleMap.animateCamera(cu);


    }

    private void showLocation() {
        try {
            if (pickUpDos.size() != 0) {
                LatLngBounds bounds;
                LatLngBounds.Builder builder;
                Double lat = 0.0;
                Double lng = 0.0;
                builder = new LatLngBounds.Builder();
                // Iterating through all the locations stored
                for (int i = 0; i < pickUpDos.size(); i++) {

                    lat = pickUpDos.get(i).lattitude;

                    // Getting the longitude of thel i-th location
                    lng = pickUpDos.get(i).longitude;
                    LogUtils.INSTANCE.debug("LATLONG", String.valueOf(lat + ":" + lng));

                    // Drawing marker on the map
                    drawMarker(new LatLng(lat, lng), pickUpDos.get(i).sequenceId);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(lat, lng), 16));

                }
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                bounds = builder.build();
//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 45);
//                googleMap.animateCamera(cu);
//                googleMap.moveCamera(CameraUpdateFactory.zoomTo(22));


            }
        } catch (Exception e) {
            Log.e(TAG, String.valueOf(R.string.fail_location), e);
        }

    }


}