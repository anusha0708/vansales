package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.R

//
class PaymentsListActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_payment, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payments)
        val btnCash = findViewById(R.id.btnCash) as Button
        btnCash.setOnClickListener {
            val intent = Intent(this@PaymentsListActivity,CashPaymentActivity::class.java)
            startActivity(intent)
        }
        val btnCheque = findViewById(R.id.btnCheque)as Button
        btnCheque.setOnClickListener {
            val intent = Intent(this@PaymentsListActivity,ChequePaymentActivity::class.java)
            startActivity(intent)
        }
    }
}