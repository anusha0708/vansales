package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatDialog
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.utils.CalendarUtils
import com.tema.vansales.vansales.utils.PreferenceUtils
import java.security.AccessController.getContext
import java.util.*

//
class PODActivity : BaseActivity() {
    lateinit var alertbox2: Dialog
    lateinit var messageTextView2: TextView
    lateinit var confirmBtn2: Button
    lateinit var cancelBtn2: Button
    lateinit var stringBuilder: StringBuilder
    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_arrival, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitleTop.setVisibility(View.VISIBLE)
        tvScreenTitleBottom.setVisibility(View.VISIBLE)

        var a = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_ID, "")
        var b = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")


        val dateTimeA  = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_AT, "")
        val dateTimeD  = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_DT, "")

        tvScreenTitleTop.setText(""+a)
        tvScreenTitleBottom.setText(""+b)

        val btnConfirm = findViewById(R.id.btnConfirmDeparture) as Button
        val tvATime = findViewById(R.id.tvATime) as TextView
        val tvATimeD = findViewById(R.id.tvATimeD) as TextView
        val tvETimeD = findViewById(R.id.tvETimeD) as TextView
        val tvETime = findViewById(R.id.tvETime) as TextView

        tvATime.setText("")
        tvATimeD.setText("")

        tvETimeD.setText(""+dateTimeD)
        tvETime.setText(""+dateTimeA)

        btnConfirm.setOnClickListener {
           // this.actualDepartureTime = this.df.format(Date())
            alertbox2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            alertbox2.requestWindowFeature(1)
            alertbox2.setContentView(R.layout.dialog_simple_alert)
            messageTextView2 = alertbox2.findViewById(R.id.tvMessage) as TextView
            confirmBtn2 = alertbox2.findViewById(R.id.btnOk) as Button
            cancelBtn2 = alertbox2.findViewById(R.id.btnNo) as Button
            (alertbox2.findViewById(R.id.tvTitle) as TextView).text = "CONFIRM DEPATRURE FROM LOCATION"
            stringBuilder = StringBuilder()
            stringBuilder.append("Have you reached "+b+"\n on ")
                stringBuilder.append(CalendarUtils.getCurrentDate())
            stringBuilder.append(" ?")
            messageTextView2.text = stringBuilder.toString()
            cancelBtn2.setOnClickListener { alertbox2.dismiss() }
            confirmBtn2.setOnClickListener {
              //  this@PODActivity.actualDepatureDate.setText(this@PODActivity.actualDepartureTime)
                alertbox2.dismiss()
                tvATimeD.setText(""+CalendarUtils.getCurrentDate())

            }
            alertbox2.show()
        }



        val btnConfirmArrival = findViewById(R.id.btnConfirmArrival) as Button

        btnConfirmArrival.setOnClickListener {

           // this.actualArrivalTime = this.df.format(Date())
            alertbox2 = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            alertbox2.requestWindowFeature(1)
            alertbox2.setContentView(R.layout.dialog_simple_alert)
            messageTextView2 = alertbox2.findViewById(R.id.tvMessage) as TextView
            confirmBtn2 = alertbox2.findViewById(R.id.btnOk) as Button
            cancelBtn2 = alertbox2.findViewById(R.id.btnNo) as Button
            (alertbox2.findViewById(R.id.tvTitle) as TextView).text = "CONFIRM ARRIVAL AT LOCATION"
            stringBuilder = StringBuilder()
            stringBuilder.append("Have you reached "+b+"\n on ")
            stringBuilder.append(dateTimeA)
            stringBuilder.append(" ?")
            messageTextView2.setText(stringBuilder.toString())
            cancelBtn2.setOnClickListener(View.OnClickListener { alertbox2.dismiss() })
            confirmBtn2.setOnClickListener(View.OnClickListener {
               // this@PODActivity.actualArrivalDate.setText(this@PODActivity.actualArrivalTime)
                alertbox2.dismiss()
            })
            alertbox2.show()  }
        val btnDeliveryDetails = findViewById(R.id.btnDeliveryDetails) as Button

        btnDeliveryDetails.setOnClickListener {
            val intent = Intent(this@PODActivity, CaptureDeliveryActivity::class.java);
            startActivity(intent);
        }
        val btnEndUnloading = findViewById(R.id.btnEndUnloading) as Button

        btnEndUnloading.setOnClickListener {
//            val intent = Intent(this@PODActivity, DashBoardActivity::class.java);
//            startActivity(intent);
        }

        val btnUnloading = findViewById(R.id.btnUnloading) as Button

        btnUnloading.setOnClickListener {
//            val intent = Intent(this@PODActivity, DashBoardActivity::class.java);
//            startActivity(intent);
            tvATime.setText(""+CalendarUtils.getCurrentDate())

        }
        val btnPayments = findViewById(R.id.btnPayments) as Button

        btnPayments.setOnClickListener {
            val intent = Intent(this@PODActivity, PaymentsActivity::class.java);
            startActivity(intent);
        }
        val btnPrintDocs = findViewById(R.id.btnPrintDocs) as Button

        btnPrintDocs.setOnClickListener {
            val intent = Intent(this@PODActivity, PrintDocumentsActivity::class.java);
            startActivity(intent);
        }
    }

}