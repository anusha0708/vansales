package com.tema.vansales.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class DriverIdMainDO implements Serializable {

    public String driverName = "";
    public String notes          = "";

    public ArrayList<DriverDO> driverDOS = new ArrayList<>();

    public ArrayList<DriverEmptyDO> driverEmptyDOS = new ArrayList<>();

}
