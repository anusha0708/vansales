package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tema.vansales.vansales.Model.LoginDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.AdminLoginRequest
import com.tema.vansales.vansales.utils.PreferenceUtils

class LoginActivity : BaseActivity() {
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String
    internal lateinit var pdbUser: String
    internal lateinit var pdbPass: String
    lateinit var llSplash: RelativeLayout

    override fun initializeControls() {
    }

    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.activity_login, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        val btnLogin = findViewById(R.id.btnLogin) as Button
        val etUserName = findViewById(R.id.etUserName) as EditText
        val etPassword = findViewById(R.id.etPassword) as EditText
//
        etUserName.setText("RENUKA")
        etPassword.setText("*U024@T3m@")

        btnLogin.setOnClickListener {
            dbUser = etUserName!!.text.toString().trim()
            dbPass = etPassword!!.text.toString().trim()
            val adminLoginRequest = AdminLoginRequest(this@LoginActivity)
            adminLoginRequest.setOnResultListener(object : AdminLoginRequest.OnResultListener {
                override fun onCompleted(isError: Boolean, isValid: Boolean, loginDO: LoginDO) {
                    if (isError) {
                        showAppCompatAlert("Alert!", ""+R.string.invalid_credentials, "OK", "", "FAILURE", false)
                    } else {
                        if (isValid) {

                            preferenceUtils = PreferenceUtils(this@LoginActivity)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
//                            preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)

                            preferenceUtils.saveString(PreferenceUtils.USER_NAME, dbUser)
                            preferenceUtils.saveString(PreferenceUtils.PASSWORD, dbPass)
                            preferenceUtils.saveString(PreferenceUtils.PROFILE_PICTURE, loginDO.Image)
                            preferenceUtils.saveString(PreferenceUtils.DRIVER_ID, loginDO.driverCode)

                            // Util.storeAdminLogin(mActivity, adminCheckBox.isChecked(), Util.USER, adminUserName, adminPass)
                            val intent = Intent(this@LoginActivity, DashBoardActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            startActivity(intent)

                        } else {
                            showAppCompatAlert("Alert!", ""+R.string.invalid_credentials, "OK", "", "FAILURE", false)
                        }
                    }
                }
            })
            adminLoginRequest.execute(dbUser, dbPass)
        }


        val ivConfiguration = findViewById(R.id.ivConfiguration) as ImageView

        ivConfiguration.setOnClickListener {
            val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java);
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent);
            finish()
        }
    }
}