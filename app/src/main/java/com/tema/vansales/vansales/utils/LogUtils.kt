package com.tema.vansales.vansales.utils

import android.util.Log


object LogUtils {
    var isLogEnabled = true

    fun error(tag: String, msg: String) {
        try {
            if (isLogEnabled)
                Log.e(tag, msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun info(tag: String, msg: String) {
        try {
            if (isLogEnabled)
                Log.i(tag, msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun verbose(tag: String, msg: String) {
        try {
            if (isLogEnabled)
                Log.v(tag, msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun debug(tag: String, msg: String) {
        try {
            if (isLogEnabled)
                Log.d(tag, msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun printMessage(msg: String) {
        try {
            if (isLogEnabled)
                println(msg)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
