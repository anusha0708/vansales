package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.CustomerListRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomerActivity extends BaseActivity {
    private String userId,orderCode;
          private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private CustomerAdapter customerAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<CustomerDo> customerDos;



    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.customer_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        preferenceUtils = new PreferenceUtils(CustomerActivity.this);

        CustomerListRequest driverListRequest = new CustomerListRequest(CustomerActivity.this);
        showLoader();
        driverListRequest.setOnResultListener(new CustomerListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, List<CustomerDo> customerDOs) {
                hideLoader();
                customerDos =customerDOs;
                if (isError) {
                    Toast.makeText(CustomerActivity.this, R.string.error_customer_list, Toast.LENGTH_SHORT).show();
                } else {

                customerAdapter= new CustomerAdapter(CustomerActivity.this,customerDos);
                    recycleview.setLayoutManager(new LinearLayoutManager(CustomerActivity.this));

                    recycleview.setAdapter(customerAdapter);

                }
            }
        });
        driverListRequest.execute();

    }

    @Override
    public void initializeControls() {
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        tvNoOrders  = (TextView) findViewById(R.id.tvNoOrders);
        tvScreenTitle.setText(R.string.customer_list);

    }

}
