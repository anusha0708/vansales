package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Model.CustomerDetailsDo;
import com.tema.vansales.vansales.Model.CustomerDetailsMainDo;
import com.tema.vansales.vansales.Model.SiteDetailsDo;
import com.tema.vansales.vansales.Model.SiteDetailsMainDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.CustomerDetailsRequest;
import com.tema.vansales.vansales.Requests.SiteDetailsRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class SiteDetailsActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private ScrollView llOrderDetails;
    private TextView siteId;
    private TextView siteName;

    private TextView siteDescription;
    private TextView legalCompany;
    private TextView stockUnit;
    ArrayList<SiteDetailsDo> siteDetailsDos;


    PreferenceUtils preferenceUtils;
    String orderCode;
    ListView listView;
    CustomAdapter customAdapter;
    @Override
    public void initialize() {
        llOrderDetails = (ScrollView) getLayoutInflater().inflate(R.layout.site_details, null);
        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeControls();
        // llshoppingCartLayout.setVisibility(View.GONE);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getIntent().hasExtra("Code")) {
            orderCode = getIntent().getExtras().getString("Code");
        }
        listView = (ListView) findViewById(R.id.lvItems);


        preferenceUtils = new PreferenceUtils(SiteDetailsActivity.this);
        SiteDetailsRequest driverListRequest = new SiteDetailsRequest(orderCode, SiteDetailsActivity.this);
        driverListRequest.setOnResultListener(new SiteDetailsRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, SiteDetailsMainDo siteDetailsMainDo) {
                if (isError) {
                    Toast.makeText(SiteDetailsActivity.this, R.string.error_site_details, Toast.LENGTH_SHORT).show();
                } else {
                    siteDetailsDos = siteDetailsMainDo.siteDetailsDos;
                    siteName.setText("" + siteDetailsMainDo.siteName);
                    siteDescription.setText("" + siteDetailsMainDo.siteDescription);
                    legalCompany.setText("" + siteDetailsMainDo.legalCompany);
                    stockUnit.setText("" + siteDetailsMainDo.stockUnit);
                    siteId.setText("" + siteDetailsMainDo.siteId);

                    if(siteDetailsMainDo.siteDetailsDos.size()>0){

                         customAdapter = new CustomAdapter(SiteDetailsActivity.this, siteDetailsDos);
                         // Set a adapter object to the listview
                         listView.setAdapter(customAdapter);
                     }


                }

            }
        });
        listView.setOnItemClickListener(this);

        driverListRequest.execute();
    }

    class CustomAdapter extends BaseAdapter {
        public Context context;
        ArrayList<SiteDetailsDo> siteDetailsDos;


        public CustomAdapter(Context context, ArrayList<SiteDetailsDo> customerDetailsDos) {

            this.context = context;
            this.siteDetailsDos = customerDetailsDos;
        }

        @Override
        public int getCount() {
            // return the all apps count
            if (siteDetailsDos.size() > 0)
                return siteDetailsDos.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflating installedapps_customlayout
            View view = getLayoutInflater().inflate(R.layout.site_list, null);

            TextView addressLine = (TextView) view.findViewById(R.id.addressLine);
            TextView addressDescription = (TextView) view.findViewById(R.id.address);
            TextView postalCode = (TextView) view.findViewById(R.id.postalCode);
            TextView city = (TextView) view.findViewById(R.id.city);
            TextView telephone = (TextView) view.findViewById(R.id.telephone);
            TextView mobile = (TextView) view.findViewById(R.id.mobile);
            TextView country = (TextView) view.findViewById(R.id.country);
            TextView countryName = (TextView) view.findViewById(R.id.countryName);
            country.setText("" + siteDetailsDos.get(position).country);
            countryName.setText("" + siteDetailsDos.get(position).countryName);
            addressLine.setText("" + siteDetailsDos.get(position).addressLine);

            addressDescription.setText("" + siteDetailsDos.get(position).address);
            postalCode.setText("" + siteDetailsDos.get(position).postalCode);
            city.setText("" + siteDetailsDos.get(position).city);
            telephone.setText("" + siteDetailsDos.get(position).telephone);
            mobile.setText("" + siteDetailsDos.get(position).mobile);

            return view;
        }

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.site_details);

        siteName = (TextView) findViewById(R.id.siteName);
        siteId = (TextView) findViewById(R.id.siteId);
        siteDescription = (TextView) findViewById(R.id.siteDescription);
        legalCompany = (TextView) findViewById(R.id.legalCompany);
        stockUnit = (TextView) findViewById(R.id.stockUnit);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
