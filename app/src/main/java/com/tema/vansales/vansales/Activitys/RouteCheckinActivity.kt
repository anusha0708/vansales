package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tema.vansales.vansales.Adapters.CreateInvoiceAdapter
import com.tema.vansales.vansales.Adapters.RouteIdsAdapter
import com.tema.vansales.vansales.Adapters.SiteAdapter
import com.tema.vansales.vansales.Model.DriverIdMainDO
import com.tema.vansales.vansales.Model.LoadStockMainDO
import com.tema.vansales.vansales.Model.PickUpDo
import com.tema.vansales.vansales.Model.SiteDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest
import com.tema.vansales.vansales.Requests.DriverIdRequest
import com.tema.vansales.vansales.Requests.SiteListRequest
import com.tema.vansales.vansales.utils.LogUtils
import com.tema.vansales.vansales.utils.PreferenceUtils


class RouteCheckinActivity : BaseActivity() {
    lateinit var tvSelection:TextView
    lateinit var dialog:Dialog
    lateinit var pickDos: DriverIdMainDO

    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.create_invoice_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.invoice)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvSelection           = findViewById(R.id.tvSelection) as TextView
        tvSelection.setText("Select Site")

        preferenceUtils = PreferenceUtils(this)

    }



}