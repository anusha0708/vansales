package com.tema.vansales.vansales.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import com.tema.vansales.vansales.R


abstract class DrawerArrowDrawable(protected var context: Context) : Drawable() {
    protected var mBarGap: Float = 0.toFloat()
    protected var mBarSize: Float = 0.toFloat()
    protected var mBarThickness: Float = 0.toFloat()
    protected var mMiddleArrowSize: Float = 0.toFloat()
    protected val mPaint = Paint()
    protected val mPath = Path()
    protected var mProgress: Float = 0.toFloat()
    protected var mSize: Int = 0
    protected var mVerticalMirror = 1f
    protected var mTopBottomArrowSize: Float = 0.toFloat()

    abstract val isLayoutRtl: Boolean

    init {
        this.mPaint.isAntiAlias = true
        this.mPaint.color = context.resources.getColor(android.R.color.white)
        this.mSize = context.resources.getDimensionPixelSize(R.dimen.ldrawer_drawableSize)
        this.mBarSize = context.resources.getDimensionPixelSize(R.dimen.ldrawer_barSize).toFloat()
        this.mTopBottomArrowSize = context.resources.getDimensionPixelSize(R.dimen.ldrawer_topBottomBarArrowSize).toFloat()
        this.mBarThickness = context.resources.getDimensionPixelSize(R.dimen.ldrawer_thickness).toFloat()
        this.mBarGap = context.resources.getDimensionPixelSize(R.dimen.ldrawer_gapBetweenBars).toFloat()
        this.mMiddleArrowSize = context.resources.getDimensionPixelSize(R.dimen.ldrawer_middleBarArrowSize).toFloat()
        this.mPaint.style = Paint.Style.STROKE
        this.mPaint.strokeJoin = Paint.Join.ROUND
        this.mPaint.strokeCap = Paint.Cap.SQUARE
        this.mPaint.strokeWidth = this.mBarThickness
    }

    protected fun lerp(paramFloat1: Float, paramFloat2: Float, paramFloat3: Float): Float {
        return paramFloat1 + paramFloat3 * (paramFloat2 - paramFloat1)
    }

    override fun draw(canvas: Canvas) {
        val localRect = bounds
        val f1 = lerp(this.mBarSize, this.mTopBottomArrowSize, this.mProgress)
        val f2 = lerp(this.mBarSize, this.mMiddleArrowSize, this.mProgress)
        val f3 = lerp(0.0f, this.mBarThickness / 2.0f, this.mProgress)
        val f4 = lerp(0.0f, ARROW_HEAD_ANGLE, this.mProgress)
        val f5 = 0.0f
        val f6 = 180.0f
        val f7 = lerp(f5, f6, this.mProgress)
        val f8 = lerp(this.mBarGap + this.mBarThickness, 0.0f, this.mProgress)
        this.mPath.rewind()
        val f9 = -f2 / 2.0f
        this.mPath.moveTo(f9 + f3, 0.0f)
        this.mPath.rLineTo(f2 - f3, 0.0f)
        val f10 = Math.round(f1 * Math.cos(f4.toDouble())).toFloat()
        val f11 = Math.round(f1 * Math.sin(f4.toDouble())).toFloat()
        this.mPath.moveTo(f9, f8)
        this.mPath.rLineTo(f10, f11)
        this.mPath.moveTo(f9, -f8)
        this.mPath.rLineTo(f10, -f11)
        this.mPath.moveTo(0.0f, 0.0f)
        this.mPath.close()
        canvas.save()
        if (!isLayoutRtl)
            canvas.rotate(180.0f, localRect.centerX().toFloat(), localRect.centerY().toFloat())
        canvas.rotate(f7 * mVerticalMirror, localRect.centerX().toFloat(), localRect.centerY().toFloat())
        canvas.translate(localRect.centerX().toFloat(), localRect.centerY().toFloat())
        canvas.drawPath(this.mPath, this.mPaint)
        canvas.restore()
    }

    override fun getIntrinsicHeight(): Int {
        return this.mSize
    }

    override fun getIntrinsicWidth(): Int {
        return this.mSize
    }

    override fun setAlpha(alpha: Int) {
        this.mPaint.alpha = alpha
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        this.mPaint.colorFilter = colorFilter
    }

    fun setVerticalMirror(mVerticalMirror: Boolean) {
        this.mVerticalMirror = (if (mVerticalMirror) 1 else -1).toFloat()
    }

    fun setProgress(paramFloat: Float) {
        this.mProgress = paramFloat
        invalidateSelf()
    }

    fun setColor(resourceId: Int) {
        this.mPaint.color = resourceId
    }

    companion object {
        private val ARROW_HEAD_ANGLE = Math.toRadians(45.0).toFloat()
    }
}