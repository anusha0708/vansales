package com.tema.vansales.vansales.listeners;

public interface LoadStockListener {
    void updateCount();
}
