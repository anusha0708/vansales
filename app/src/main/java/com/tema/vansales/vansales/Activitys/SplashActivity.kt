package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.os.Handler
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout


import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.utils.PreferenceUtils

class SplashActivity : BaseActivity() {

    lateinit var llSplash: RelativeLayout
    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.dashboard, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()


        Handler().postDelayed({
            preferenceUtils = PreferenceUtils(this@SplashActivity)

            if (preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "") .equals("")|| preferenceUtils.getStringFromPreference(PreferenceUtils.PASSWORD, "").equals("")) {
                intent = Intent(this@SplashActivity, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
                finish()
            } else {
                intent = Intent(this@SplashActivity, DashBoardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("From", "Login")
                startActivity(intent)
                //overridePendingTransition(R.anim.enter, R.anim.exit)
                finish()
            }
//            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//            startActivity(intent)
//            finish()
        }, 2000)

    }

    override fun initializeControls() {
        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

    }

//    public override fun onResume() {
//        super.onResume()
//
//        Handler().postDelayed({
//            val intent = Intent(this@SplashActivity, LoginActivity::class.java)
//            startActivity(intent)
//        }, 2000)
//
//    }
}
