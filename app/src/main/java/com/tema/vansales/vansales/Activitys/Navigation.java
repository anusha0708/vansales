package com.tema.vansales.vansales.Activitys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest;
import com.tema.vansales.vansales.utils.LogUtils;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;


/**
 * Created by sandy on 2/15/2018.
 */

public class Navigation extends BaseActivity implements OnMapReadyCallback {
    private LinearLayout llAboutUs;
    Double address;

    private static final String TAG = CurrentMapActivity.class.getSimpleName();

    private ImageButton backButton;
    String code;
    public GoogleMap googleMap;
    View rootView;



    @Override
    public void onMapReady(GoogleMap googlemap) {
        googleMap = googlemap;
        if (googleMap == null) {
            Toast.makeText(Navigation.this, "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true);

        }
        try {
            showLocation();

        } catch (Exception e) {
            Log.e(TAG, "Failed to initialize", e);
        }


    }

    private void drawMarker(LatLng point){
        LatLngBounds.Builder builder;
        CameraUpdate cu; // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();
        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);

//        googleMap.moveCamera(cu);
//        googleMap.animateCamera(cu);


    }
    private void showLocation() {
        try {

                LatLngBounds bounds;
                LatLngBounds.Builder builder;
                Double lat = 0.0;
                Double lng =0.0;
                builder = new LatLngBounds.Builder();
                // Iterating through all the locations stored
             lat = Double.valueOf(preferenceUtils.getStringFromPreference(PreferenceUtils.LATTITUDE, ""));
             lng = Double.valueOf(preferenceUtils.getStringFromPreference(PreferenceUtils.LATTITUDE, ""));
                    drawMarker(new LatLng(lat, lng));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(lat,lng), 13));


                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                bounds = builder.build();
//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 45);
//                googleMap.animateCamera(cu);
//                googleMap.moveCamera(CameraUpdateFactory.zoomTo(22));



        } catch (Exception e) {
            Log.e(TAG, "Failed to update location", e);
        }

    }

    @Override
    public void initialize() {
      LinearLayout  llOrderDetails = (LinearLayout) getLayoutInflater().inflate(R.layout.navigation, null);
        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.navigation);
        if (getIntent().hasExtra("Code")) {
            code =getIntent().getExtras().getString("Code");
        }
        PreferenceUtils  preferenceUtils = new PreferenceUtils(Navigation.this);

        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.LATTITUDE, "");
        if(id.length()>0) {

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(Navigation.this);
//                    VehicleRouteAdapter driverAdapter = new VehicleRouteAdapter(getActivity(), pods);
//                    recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//                    recycleview.setAdapter(driverAdapter);

        }
        else {
            Toast.makeText(Navigation.this, "No Routing Id's Found", Toast.LENGTH_SHORT).show();
        }
    }
}