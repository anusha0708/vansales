package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tema.vansales.vansales.Model.LoadStockDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.common.AppConstants;
import com.tema.vansales.vansales.listeners.LoadStockListener;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class CurrentStockAdapter extends RecyclerView.Adapter<CurrentStockAdapter.MyViewHolder> /*implements Filterable*/ {
    private ArrayList<LoadStockDO> loadStockDOS;
    private String imageURL;
    private Context context;
    private PreferenceUtils preferenceUtils;
    private LoadStockListener loadStockListener;
    double    mass =0.0;

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    listLunchDos = listLunchDos;
//                } else {
//                    ArrayList<LunchDo> filteredList = new ArrayList<>();
//                    for (LunchDo row : listLunchDos) {
//
//                        if (row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }
//                    }
//
//                    listLunchDos = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = listLunchDos;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                listLunchDos = (ArrayList<LunchDo>) filterResults.values;
//                notifyDataSetChanged();
//            }
//        };
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription,tvNumber;
        public RelativeLayout rlRemove,rlAdd;
        public ImageView ivRemove,ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = (TextView) view.findViewById(R.id.tvName);
            tvDescription   = (TextView) view.findViewById(R.id.tvDescription);
//            rlRemove        = (RelativeLayout) view.findViewById(R.id.rlRemove);
//            rlAdd           = (RelativeLayout) view.findViewById(R.id.rlAdd);
            ivRemove        = (ImageView) view.findViewById(R.id.ivRemove);
            ivAdd           = (ImageView) view.findViewById(R.id.ivAdd);
            tvNumber        = (TextView) view.findViewById(R.id.tvNumber);
        }
    }


    public CurrentStockAdapter(Context context, ArrayList<LoadStockDO> loadStockDoS, LoadStockListener listener) {
        this.context = context;
        this.loadStockDOS = loadStockDoS;
        this.loadStockListener = listener;
        preferenceUtils = new PreferenceUtils(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.load_stock_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final LoadStockDO loadStockDO = AppConstants.listStockDO.get(position);
        holder.tvProductName.setText(loadStockDO.product);
        holder.tvDescription.setText(loadStockDO.productDescription + " Stock:" +loadStockDO.quantity);
        holder.tvNumber.setText(""+loadStockDO.itemCount);
//
//
//        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                AppConstants.listStockDO.get(position).itemCount  =    AppConstants.listStockDO.get(position).itemCount + 1;
//
//                holder.tvNumber.setText(""+   AppConstants.listStockDO.get(position).itemCount);
//              //                          mass = mass +  AppConstants.listStockDO.get(position).itemCount*AppConstants.listStockDO.get(position).stockUnit;
//
//
//                int cartCount = getCount();
//                preferenceUtils.saveInt(PreferenceUtils.STOCK_COUNT,cartCount);
//              loadStockListener.updateCount();
//
////                         count= count+1;
////                holder.tvNumber.setText(""+count);
//
//            }
//        });
//        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if(   AppConstants.listStockDO.get(position).itemCount > 0){
//                    AppConstants.listStockDO.get(position).itemCount=    AppConstants.listStockDO.get(position).itemCount- 1;
//
//                    holder.tvNumber.setText(""+   AppConstants.listStockDO.get(position).itemCount);
//                    int cartCount = getCount();
//                    preferenceUtils.saveInt(PreferenceUtils.STOCK_COUNT,cartCount);
//                    loadStockListener.updateCount();
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return loadStockDOS.size();
    }

    private int getCount(){

        int count = 0;
        for(LoadStockDO loadStockDO : AppConstants.listStockDO){
            if(loadStockDO.itemCount > 0){
                count = count + 1;
            }
        }
        return count;
    }

}
