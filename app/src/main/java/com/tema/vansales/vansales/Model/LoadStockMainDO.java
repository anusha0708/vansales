package com.tema.vansales.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class LoadStockMainDO implements Serializable {

    public String mass = "";
    public String volume = "";

    public String massUnit = "";
    public String volumeUnit = "";

    public String vanMass = "";
    public String vanVolume = "";


    public ArrayList<LoadStockDO> loadStockDOS = new ArrayList<>();


}
