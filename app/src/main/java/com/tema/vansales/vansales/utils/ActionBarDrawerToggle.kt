package com.tema.vansales.vansales.utils

import android.app.ActionBar
import android.app.Activity
import android.content.res.Configuration
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView


open class ActionBarDrawerToggle
//    public ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar drawerImageRes, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
//        super(activity, drawerLayout, drawerImageRes, openDrawerContentDescRes, closeDrawerContentDescRes);
//    }

(protected var mActivity: Activity?, protected var mDrawerLayout: DrawerLayout, toolbar: Toolbar, protected var mOpenDrawerContentDescRes: Int, protected var mCloseDrawerContentDescRes: Int, protected var mDrawerImage: DrawerArrowDrawable?) : android.support.v7.app.ActionBarDrawerToggle(mActivity, mDrawerLayout, toolbar, mOpenDrawerContentDescRes, mCloseDrawerContentDescRes) {
    var isAnimateEnabled: Boolean = false

    init {
        isAnimateEnabled = true
    }//    	ActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, DrawerArrowDrawable drawerImage, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
    //        super(activity, drawerLayout, R.drawable.ic_drawer, openDrawerContentDescRes, closeDrawerContentDescRes);

    override fun syncState() {
        if (mDrawerImage == null) {
            super.syncState()
            return
        }
        if (isAnimateEnabled) {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerImage!!.setProgress(1f)
            } else {
                mDrawerImage!!.setProgress(0f)
            }
        }
        //        setActionBarUpIndicator();
        //        setActionBarDescription();
    }

    override fun setDrawerIndicatorEnabled(enable: Boolean) {
        if (mDrawerImage == null) {
            super.setDrawerIndicatorEnabled(enable)
            return
        }
        setActionBarUpIndicator()
        setActionBarDescription()
    }

    override fun isDrawerIndicatorEnabled(): Boolean {
        return if (mDrawerImage == null) {
            super.isDrawerIndicatorEnabled()
        } else true
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        if (mDrawerImage == null) {
            super.onConfigurationChanged(newConfig)
            return
        }
        syncState()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        if (mDrawerImage == null) {
            super.onDrawerSlide(drawerView, slideOffset)
            return
        }
        if (isAnimateEnabled) {
            mDrawerImage!!.setVerticalMirror(!mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerImage!!.setProgress(slideOffset)
        }
    }

    override fun onDrawerOpened(drawerView: View) {
        if (mDrawerImage == null) {
            super.onDrawerOpened(drawerView)
            return
        }
        if (isAnimateEnabled) {
            mDrawerImage!!.setProgress(1f)
        }
        setActionBarDescription()
    }

    override fun onDrawerClosed(drawerView: View) {
        if (mDrawerImage == null) {
            super.onDrawerClosed(drawerView)
            return
        }
        if (isAnimateEnabled) {
            mDrawerImage!!.setProgress(0f)
        }
        setActionBarDescription()
    }

    protected fun setActionBarUpIndicator() {
        if (mActivity != null) {
            try {
                val setHomeAsUpIndicator = ActionBar::class.java.getDeclaredMethod("setHomeAsUpIndicator",
                        Drawable::class.java)
                setHomeAsUpIndicator.invoke(mActivity!!.actionBar, mDrawerImage)
                return
            } catch (e: Exception) {
                Log.e(TAG, "setActionBarUpIndicator error", e)
            }

            val home = mActivity!!.findViewById<View>(android.R.id.home) ?: return

            val parent = home.parent as ViewGroup
            val childCount = parent.childCount
            if (childCount != 2) {
                return
            }

            val first = parent.getChildAt(0)
            val second = parent.getChildAt(1)
            val up = if (first.id == android.R.id.home) second else first

            if (up is ImageView) {
                up.setImageDrawable(mDrawerImage)
            }
        }
    }

    protected fun setActionBarDescription() {
        if (mActivity != null && mActivity!!.actionBar != null) {
            try {
                val setHomeActionContentDescription = ActionBar::class.java.getDeclaredMethod(
                        "setHomeActionContentDescription", Integer.TYPE)
                setHomeActionContentDescription.invoke(mActivity!!.actionBar,
                        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) mOpenDrawerContentDescRes else mCloseDrawerContentDescRes)
                if (Build.VERSION.SDK_INT <= 19) {
                    mActivity!!.actionBar!!.subtitle = mActivity!!.actionBar!!.subtitle
                }
            } catch (e: Exception) {
                Log.e(TAG, "setActionBarUpIndicator", e)
            }

        }
    }

    companion object {

        private val TAG = ActionBarDrawerToggle::class.java.name
    }

}
