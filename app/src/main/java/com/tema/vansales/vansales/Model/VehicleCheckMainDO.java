package com.tema.vansales.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class VehicleCheckMainDO implements Serializable {

    public String flag = "";
    public String driverId = "";
    public String routingId = "";
    public String vehicleCode = "";
    public String itemsInVehicle = "";
    public String safetyEquipment = "";
    public String vehicleDocument = "";


}
