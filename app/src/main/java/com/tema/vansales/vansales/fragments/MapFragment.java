package com.tema.vansales.vansales.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.Activitys.CurrentMapActivity;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest;
import com.tema.vansales.vansales.utils.LogUtils;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;


/**
 * Created by sandy on 2/15/2018.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {
    private LinearLayout llAboutUs;
    Double address;

    private static final String TAG = CurrentMapActivity.class.getSimpleName();

    private ImageButton backButton;
    String code;
    public GoogleMap googleMap;
    ArrayList<PickUpDo> pickUpDos;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         rootView = inflater.inflate(R.layout.map, container, false);
        if (getActivity().getIntent().hasExtra("Code")) {
            code =getActivity(). getIntent().getExtras().getString("Code");
        }
      PreferenceUtils  preferenceUtils = new PreferenceUtils(getActivity());

        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
           if(id.length()>0){
               DisplayPickupListRequest driverListRequest = new DisplayPickupListRequest(getActivity(), id);

               driverListRequest.setOnResultListener(new DisplayPickupListRequest.OnResultListener() {
                   @Override
                   public void onCompleted(boolean isError, ArrayList<PickUpDo> pods) {
                       //((BaseActivity)conhideLoader();
                       pickUpDos = pods;
                       if (isError) {
                           Toast.makeText(getActivity(), "Failed to get route list", Toast.LENGTH_SHORT).show();
                       } else {
                           SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

                           mapFragment.getMapAsync(MapFragment.this);
//                    VehicleRouteAdapter driverAdapter = new VehicleRouteAdapter(getActivity(), pods);
//                    recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));
//
//                    recycleview.setAdapter(driverAdapter);

                       }


                   }
               });
               driverListRequest.execute();
           }else {
               Toast.makeText(getActivity(), "No Routing Id's Found", Toast.LENGTH_SHORT).show();
           }

        return rootView;
    }

    @Override
    public void onMapReady(GoogleMap googlemap) {
        googleMap = googlemap;
        if (googleMap == null) {
            Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true);

        }
        try {
            showLocation();

        } catch (Exception e) {
            Log.e(TAG, "Failed to initialize", e);
        }


    }

    private void drawMarker(LatLng point, String id){
        LatLngBounds.Builder builder;
        CameraUpdate cu; // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();
        // Setting latitude and longitude for the marker
        markerOptions.position(point);
        markerOptions.position(point).title("shipmentID :").snippet(id);

        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);

//        googleMap.moveCamera(cu);
//        googleMap.animateCamera(cu);


    }
    private void showLocation() {
        try {
            if(pickUpDos.size()!=0){
                LatLngBounds bounds;
                LatLngBounds.Builder builder;
                Double lat = 0.0;
                Double lng =0.0;
                builder = new LatLngBounds.Builder();
                // Iterating through all the locations stored
                for(int i=0;i<pickUpDos.size();i++) {

                    lat = pickUpDos.get(i).lattitude;

                    // Getting the longitude of thel i-th location
                    lng = pickUpDos.get(i).longitude;
                    LogUtils.INSTANCE.debug("LATLONG", String.valueOf(lat+":"+lng));

                    // Drawing marker on the map
                    drawMarker(new LatLng(lat, lng),pickUpDos.get(i).sequenceId);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(lat,lng), 6));

                }
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);

//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 45);
//                googleMap.animateCamera(cu);
//                googleMap.moveCamera(CameraUpdateFactory.zoomTo(22));



            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to update location", e);
        }

    }

}