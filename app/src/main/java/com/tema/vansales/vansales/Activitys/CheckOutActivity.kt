package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.utils.PreferenceUtils

//
class CheckOutActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_check_out, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.check_out)
        val btnCheckout = findViewById(R.id.btnCheckout) as Button
        preferenceUtils = PreferenceUtils(this@CheckOutActivity)
        val id = findViewById(R.id.id) as TextView
         id.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, ""))
        btnCheckout.setOnClickListener {
            val intent = Intent(this@CheckOutActivity, DashBoardActivity::class.java);
            startActivity(intent);
        }
    }
}