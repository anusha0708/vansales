package com.tema.vansales.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.Model.CreatePaymentDO;
import com.tema.vansales.vansales.Model.CustomerDetailsDo;
import com.tema.vansales.vansales.Model.CustomerDetailsMainDo;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CreatePaymentRequest extends AsyncTask<String, Void, Boolean> {

    private CreatePaymentDO createPaymentDO;
    private Context mContext;
    private String id;
    String username,password,ip,pool,port;
    PreferenceUtils preferenceUtils;

    public CreatePaymentRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, CreatePaymentDO createPaymentDO);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customerId);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://"+ip+":"+port+"/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", "X10CSAPAY2");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YSINVNO", id);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser",username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" +password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createPaymentDO = new CreatePaymentDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    }
                    else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                    else if (startTag.equalsIgnoreCase("TAB")) {
                    //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    }else if (startTag.equalsIgnoreCase("LIN")) {
                  //      createPaymentDO = new CustomerDetailsDo();

                    }
                }
                else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YPAYNO")) {
                            createPaymentDO.paymentNumber=text;


                        } else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            createPaymentDO.site=text;

                        } else if (attribute.equalsIgnoreCase("O_YBP")) {
                            createPaymentDO.customer=text;

                        } else if (attribute.equalsIgnoreCase("O_YBPRSAC")) {
                            createPaymentDO.control=text;

                        } else if (attribute.equalsIgnoreCase("O_YACC")) {
                            createPaymentDO.account=text;

                        } else if (attribute.equalsIgnoreCase("O_YACCDAT")) {
                            createPaymentDO.accountingDate=text;

                        } else if (attribute.equalsIgnoreCase("O_YBAN")) {
                            createPaymentDO.bank=text;

                        } else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            createPaymentDO.currency=text;

                        } else if (attribute.equalsIgnoreCase("O_YBPAMT")) {
                            createPaymentDO.amount=text;

                        } else if (attribute.equalsIgnoreCase("O_YCHQNUM")) {
                            createPaymentDO.checkNumber=text;

                        } else if (attribute.equalsIgnoreCase("O_YBPAINV")) {
                            createPaymentDO.address=text;

                        } else if (attribute.equalsIgnoreCase("O_YBANAMT")) {
                            createPaymentDO.bankAmount=text;

                        }
                    }


                     if (endTag.equalsIgnoreCase("GRP")) {
                       // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                       // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createPaymentDO);
        }
    }
}