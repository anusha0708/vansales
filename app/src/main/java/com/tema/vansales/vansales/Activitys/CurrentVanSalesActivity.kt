package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tema.vansales.vansales.Adapters.CurrentStockAdapter
import com.tema.vansales.vansales.Adapters.LoadStockAdapter
import com.tema.vansales.vansales.Model.DriverIdMainDO
import com.tema.vansales.vansales.Model.LoadStockDO
import com.tema.vansales.vansales.Model.LoadStockMainDO
import com.tema.vansales.vansales.Model.PriceItemDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.DriverIdRequest
import com.tema.vansales.vansales.Requests.LoadVanSaleRequest
import com.tema.vansales.vansales.common.AppConstants
import com.tema.vansales.vansales.listeners.LoadStockListener
import com.tema.vansales.vansales.utils.PreferenceUtils
import java.util.ArrayList

//
class CurrentVanSalesActivity : BaseActivity(), LoadStockListener {
    override fun updateCount() {
        var count = preferenceUtils.getIntFromPreference(PreferenceUtils.STOCK_COUNT, 0)
//        etLoadingMass.setText("" + count)
//        etLoadingVolume.setText("" + mass)

    }

    var mass = 0.00

    lateinit var recycleview: RecyclerView
    lateinit var loadStockAdapter: CurrentStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var username: String
    lateinit var loadStockDO: LoadStockDO
    lateinit var etLoadingMass: EditText
    lateinit var etLoadingVolume: EditText
    lateinit var tvVehicleMass: TextView
    lateinit var tvVehicleVolume: TextView
    lateinit var loadStockMainDO: LoadStockMainDO
    lateinit var tvMass: TextView
    lateinit var tvVolume: TextView

    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.fragment_van_sales, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        preferenceUtils = PreferenceUtils(this@CurrentVanSalesActivity)
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        val loadVanSaleRequest = LoadVanSaleRequest(username, this@CurrentVanSalesActivity)
        //  showLoader();
        loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
            hideLoader()

            loadStockMainDO = loadStockMainDo
            if (isError) {
                Toast.makeText(this@CurrentVanSalesActivity, R.string.error_checkin, Toast.LENGTH_SHORT).show()
            } else {
                if (loadStockMainDo.loadStockDOS.size > 0) {
                    val contacts = ArrayList<LoadStockDO>()
                    AppConstants.listStockDO = loadStockMainDo.loadStockDOS
                    loadStockAdapter = CurrentStockAdapter(this@CurrentVanSalesActivity, loadStockMainDo.loadStockDOS, this@CurrentVanSalesActivity)
                    recycleview.setAdapter(loadStockAdapter)
                    tvVehicleMass.setText("" + loadStockMainDo.mass)
                    tvVehicleVolume.setText("" + loadStockMainDo.volume)
                    etLoadingMass.setText("" + loadStockMainDo.vanMass)
                    tvMass.setText("Mass(" + loadStockMainDo.massUnit+")")
                    tvVolume.setText("Volume(" + loadStockMainDo.volumeUnit+")")
                    etLoadingVolume.setText("" + loadStockMainDo.vanVolume)
//                    for (loadStockDo in AppConstants.listStockDO) {
////                        mass = mass + (loadStockDo.itemCount*loadStockDo.productVolume)!!
////                        mass = mass + (loadStockDo.productWeight*loadStockDo.productWeight)!!
//
//                        etLoadingVolume.setText(""+mass)
//                    }


                } else {
                    showAlert("No Routing Id's Found")
                }


                //ArrayAdapter adapter = new ArrayAdapter(CheckinActivity.this, android.R.layout.simple_spinner_item, country);
                //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //spinner.setAdapter(adapter);

            }
        }
        loadVanSaleRequest.execute()


    }

    override fun initializeControls() {
        tvScreenTitle.setText(R.string.current_stock)
        recycleview = findViewById(R.id.recycleview) as RecyclerView
        etLoadingMass = findViewById(R.id.etLoadingMass) as EditText
        etLoadingVolume = findViewById(R.id.etLoadingVolume) as EditText
        tvVehicleMass = findViewById(R.id.tvVehicleMass) as TextView
        tvVehicleVolume = findViewById(R.id.tvVehicleVolume) as TextView
        tvMass = findViewById(R.id.tvMass) as TextView
        tvVolume = findViewById(R.id.tvVolume) as TextView

        val linearLayoutManager = LinearLayoutManager(this)
        recycleview.setLayoutManager(linearLayoutManager)

//        val btnLoad = findViewById(R.id.btnLoad) as Button
//
//        btnLoad.setOnClickListener {
//            val intent = Intent(this@LoadVanSalesActivity, DashBoardActivity::class.java);
//            startActivity(intent);
//        }
    }
}

private operator fun String.times(totalStockVolume: Double?): Double? {
    return totalStockVolume
}

