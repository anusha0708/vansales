package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.tema.vansales.vansales.Adapters.CreateInvoiceAdapter
import com.tema.vansales.vansales.Adapters.CreatePaymentAdapter
import com.tema.vansales.vansales.Model.CreatePaymentDO
import com.tema.vansales.vansales.Model.SiteDO
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.CreatePaymentRequest
import com.tema.vansales.vansales.Requests.SiteListRequest
import com.tema.vansales.vansales.utils.PreferenceUtils


class CreatePaymentActivity : BaseActivity() {

    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var createPaymentDo: CreatePaymentDO
    override protected fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.create_payments_screen, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvSelection = findViewById(R.id.tvSelection) as TextView
        tvSelection.setText("Select Site")
        preferenceUtils = PreferenceUtils(this)
        //   tvSelection.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, ""))


        tvScreenTitle.setText(R.string.payment)
        tvSelection.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, ""))
        var id = tvSelection.text.toString()
        var btnPayments = findViewById(R.id.btnPayments) as TextView
        btnPayments.setOnClickListener {


            val siteListRequest = CreatePaymentRequest(id, this@CreatePaymentActivity)
            siteListRequest.setOnResultListener { isError, createPaymentDO ->
                hideLoader()
                createPaymentDo = createPaymentDO
                if (isError) {
                    Toast.makeText(this@CreatePaymentActivity, R.string.error_site_list, Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this@CreatePaymentActivity, "Payment Created", Toast.LENGTH_SHORT).show()

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

                }
            }

            siteListRequest.execute()
        }

        tvSelection.setOnClickListener {
            dialog = Dialog(this, R.style.NewDialog)
            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            dialog.setContentView(R.layout.invoice_list_dialog)
            val window = dialog.getWindow()
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

            var recyclerView = dialog.findViewById(R.id.recycleview) as RecyclerView
            recyclerView.setLayoutManager(LinearLayoutManager(this));

//            var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)

            dialog.show()


        }
    }

}