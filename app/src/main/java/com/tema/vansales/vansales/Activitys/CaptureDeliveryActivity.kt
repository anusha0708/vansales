package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.fragments.CaptureDeliveryFragment

//
class CaptureDeliveryActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_capture_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.capture_delivery_details)
        val btnScan = findViewById(R.id.btnScan) as Button

        btnScan.setOnClickListener {
            val intent = Intent(this@CaptureDeliveryActivity, ScanProductsActivity::class.java);
            startActivity(intent);
        }
        val btnCompleted = findViewById(R.id.btnCompleted) as Button

        btnCompleted.setOnClickListener {
           finish()
        }
    }

}