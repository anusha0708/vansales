package com.tema.vansales.vansales.Requests;

import android.os.AsyncTask;


import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.ServiceURLS;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class ConfigRequest extends AsyncTask<String, Void, Boolean> {
    boolean isValid = false;

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void OnCompleted(boolean isError, boolean isSuccess);
    }

    public boolean runRequest(String address, String port, String pool, String username, String Password) {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        String URL = "http://" + address + ":" + port +ServiceURLS.APP_SUB_URL;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", "X10AADMIN");
        JSONObject jsonObject = new JSONObject();
        try {


            jsonObject.put("XUNAME", username);
            jsonObject.put("XPWD", Password);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", Password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

//        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;


        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + Password).getBytes())));

            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("XFLG")) {
                            if (text.equalsIgnoreCase("2")) {
                                isValid = true;
                            }
                        }
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }

            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);
            return false;
        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest(param[0], param[1], param[2], param[3], param[4]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (onResultListener != null) {
            onResultListener.OnCompleted(!result, isValid);
        }
    }
}