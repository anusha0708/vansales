package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tema.vansales.vansales.Activitys.CheckinActivity;
import com.tema.vansales.vansales.Activitys.CreateInvoiceActivity;
import com.tema.vansales.vansales.Activitys.RouteCheckinActivity;
import com.tema.vansales.vansales.Model.DriverDO;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class RouteIdsAdapter extends RecyclerView.Adapter<RouteIdsAdapter.MyViewHolder>  {

    private ArrayList<DriverDO> pickUpDos;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvSiteName = (TextView) view.findViewById(R.id.tvSiteName);
            tvSiteId = (TextView) view.findViewById(R.id.tvSiteId);



        }
    }


    public RouteIdsAdapter(Context context, ArrayList<DriverDO> siteDOS) {
        this.context = context;
        this.pickUpDos = siteDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.checkin_dialog_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DriverDO siteDO = pickUpDos.get(position);
        holder.tvSiteName.setText(siteDO.vehicleRouteId);
      //  holder.tvSiteId.setText("" + siteDO.siteId);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, String.valueOf(pickUpDos.get(pos).vehicleCarrier));

                preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, String.valueOf(pickUpDos.get(pos).vehicleRouteId));
                ((CheckinActivity)context).tvSelection.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, ""));
                ((CheckinActivity)context).dialog.dismiss();


            }
        });


    }

    @Override
    public int getItemCount() {
        return pickUpDos.size();
    }

}
