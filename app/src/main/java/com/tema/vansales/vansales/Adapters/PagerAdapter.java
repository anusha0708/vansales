package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/15/2018.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import com.tema.vansales.vansales.fragments.MapFragment;
import com.tema.vansales.vansales.fragments.RouteListFragmment;

public class PagerAdapter extends FragmentStatePagerAdapter  {

    public PagerAdapter(FragmentManager fm) {
        super(fm);



    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RouteListFragmment vegFragment = new RouteListFragmment();

                return vegFragment;
            case 1:
                MapFragment nonVegFragment = new MapFragment();

                return nonVegFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }


}
