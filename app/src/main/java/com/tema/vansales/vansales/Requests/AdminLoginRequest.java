package com.tema.vansales.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;


import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.Model.LoginDO;
import com.tema.vansales.vansales.Model.SiteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.PreferenceUtils;
import com.tema.vansales.vansales.utils.ProgressTask;
import com.tema.vansales.vansales.utils.ServiceURLS;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


public class AdminLoginRequest extends AsyncTask<String, Void, Boolean> {
    boolean isValid = false;
    private Context mContext;
    String username, passWord, ip, pool, port;
    PreferenceUtils preferenceUtils;
    LoginDO loginDO ;
    public AdminLoginRequest(Context mContext) {

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, boolean isValid,LoginDO loginDO);

    }

    public boolean runRequest(String userID, String password) {

        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "");
        passWord = preferenceUtils.getStringFromPreference(PreferenceUtils.PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");


        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String URL = "http://"+ip+":"+port+"/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", "X10AADMIN");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("XUNAME", userID);
            jsonObject.put("XPWD", password);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser",username);
        callcontext.addProperty("password", passWord);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + passWord).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        try {
            String text = "", attribute = "", startTag = "", attribute2 = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            loginDO = new LoginDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("XFLG")) {
                            if (text.equalsIgnoreCase("2")) {
                                isValid = true;
                            }
                        }
                        if (attribute.equalsIgnoreCase("O_YDIMG")) {

                            loginDO.Image= text;

                        }
                        if (attribute.equalsIgnoreCase("XDNAME")) {

                            loginDO.driverCode= text;

                        }
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();
      //  ProgressTask.getInstance().showProgress(mContext, false, "Checking Credentials...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest(param[0], param[1]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity)mContext).hideLoader();
        if (onResultListener != null ) {
            if(loginDO!=null){

                onResultListener.onCompleted(!result, isValid,loginDO);
            }
            else {
                ((BaseActivity)mContext).showAppCompatAlert("Alert!", "Invalid Credentials", "OK", "", "FAILURE", false);
            }
        }
    }
}