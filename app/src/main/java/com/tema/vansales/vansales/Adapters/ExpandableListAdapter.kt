package com.tema.vansales.vansales.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.tema.vansales.vansales.R

import com.tema.vansales.vansales.utils.MenuModel

import java.util.HashMap

class ExpandableListAdapter(private val context: Context, private val listDataHeader: List<MenuModel>, private val listDataChild: HashMap<MenuModel, List<MenuModel>>) : BaseExpandableListAdapter() {

    override fun getChild(groupPosition: Int, childPosititon: Int): MenuModel {
        return (this.listDataChild[this.listDataHeader[groupPosition]] as List<*>)[childPosititon] as MenuModel
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    @SuppressLint("WrongConstant")
    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val childText = getChild(groupPosition, childPosition).menuName
        if (convertView == null) {
            convertView = (this.context.getSystemService("layout_inflater") as LayoutInflater).inflate(R.layout.list_group_child, null)
        }
        (convertView!!.findViewById<View>(R.id.tvItem) as TextView).text = childText
        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return if (this.listDataChild[this.listDataHeader[groupPosition]] == null) {
            0
        } else (this.listDataChild[this.listDataHeader[groupPosition]] as List<*>).size
    }

    override fun getGroup(groupPosition: Int): MenuModel {
        return this.listDataHeader[groupPosition]
    }

    override fun getGroupCount(): Int {
        return this.listDataHeader.size
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    @SuppressLint("WrongConstant")
    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val headerTitle = getGroup(groupPosition).menuName
        if (convertView == null) {
            convertView = (this.context.getSystemService("layout_inflater") as LayoutInflater).inflate(R.layout.list_group_header, null)
        }
        val lblListHeader = convertView!!.findViewById<View>(R.id.tvHeader) as TextView
        lblListHeader.setTypeface(null, 1)
        lblListHeader.text = headerTitle
        val ind = convertView.findViewById<View>(R.id.iv)
        if (ind != null) {
            val indicator = ind as ImageView
            if (getChildrenCount(groupPosition) == 0) {
                indicator.visibility = 4
            } else {
               // indicator.visibility = View.GONE
                indicator.visibility = 0
            //    indicator.drawable.state = GROUP_STATE_SETS[isExpanded]
            }
        }
        return convertView
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    companion object {
        private val EMPTY_STATE_SET = IntArray(0)
        private val GROUP_EXPANDED_STATE_SET = intArrayOf(16842920)
        private val GROUP_STATE_SETS = arrayOf(EMPTY_STATE_SET, GROUP_EXPANDED_STATE_SET)
    }
}
