package com.tema.vansales.vansales.Activitys;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Adapters.SiteAdapter;
import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.Model.SiteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.CustomerListRequest;
import com.tema.vansales.vansales.Requests.SiteListRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.List;

public class SiteActivity extends BaseActivity {
    private RecyclerView recycleview;
    private SiteAdapter siteAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoItems;
    private List<SiteDO> siteDOS;



    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.site_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycleview.setLayoutManager(new LinearLayoutManager(this));


        SiteDO siteDO = new SiteDO();
        SiteListRequest siteListRequest = new SiteListRequest(SiteActivity.this);
        showLoader();
        siteListRequest.setOnResultListener(new SiteListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, List<SiteDO> siteDoS) {
                hideLoader();
                siteDOS =siteDoS;
                if (isError) {
                    Toast.makeText(SiteActivity.this, R.string.error_site_list, Toast.LENGTH_SHORT).show();
                } else {

                    siteAdapter= new SiteAdapter(SiteActivity.this,siteDOS);
                    recycleview.setAdapter(siteAdapter);

                }
            }
        });

        siteListRequest.execute();

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.site_list);

        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        tvNoItems  = (TextView) findViewById(R.id.tvNoOrders);

    }

}
