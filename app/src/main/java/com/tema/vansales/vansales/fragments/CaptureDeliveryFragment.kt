package com.tema.vansales.vansales.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView

import com.tema.vansales.vansales.R


class CaptureDeliveryFragment : Fragment(), OnClickListener {
    private var barcode: ImageView? = null
    private var capture_delivery2_completed_button: Button? = null
    private var mListener: OnFragmentInteractionListener? = null
    private var mParam1: String? = null
    private var mParam2: String? = null
    private var minteger1 = 0
    private var minusButton1: Button? = null
    private var plusButton1: Button? = null
    private var stock1_ET: EditText? = null
    private var stock1_TV: TextView? = null

    /* renamed from: test.com.screensdesign.fragments.CaptureDeliveryFragment$1 */
    internal inner class C04041 : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun afterTextChanged(editable: Editable) {
            this@CaptureDeliveryFragment.stock1_TV!!.text = this@CaptureDeliveryFragment.stock1_ET!!.text
        }
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            this.mParam1 = arguments!!.getString(ARG_PARAM1)
            this.mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_capture_delivery_fragment2, container, false)
        this.plusButton1 = view.findViewById<View>(R.id.capture_delivery2_stock1_plus_button) as Button
        this.minusButton1 = view.findViewById<View>(R.id.capture_delivery2_stock1_minus_button) as Button
        this.stock1_ET = view.findViewById<View>(R.id.capture_delivery2_stock1_number_display) as EditText
        this.stock1_TV = view.findViewById<View>(R.id.capture_delivery2_stock) as TextView
        this.barcode = view.findViewById<View>(R.id.barcode) as ImageView
        this.capture_delivery2_completed_button = view.findViewById<View>(R.id.btnConfirm) as Button
        this.plusButton1!!.setOnClickListener(this)
        this.minusButton1!!.setOnClickListener(this)
        this.capture_delivery2_completed_button!!.setOnClickListener(this)
        //  AndroidBarcodeView androidBarcodeView = new AndroidBarcodeView(getContext(), "12345678");
        this.stock1_ET!!.addTextChangedListener(C04041())
        return view
    }

    fun onButtonPressed(uri: Uri) {
        if (this.mListener != null) {
            this.mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            this.mListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.mListener = null
    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.btnConfirm) {
            activity!!.onBackPressed()
        } else if (id != R.id.capture_delivery2_stock1_minus_button) {
            if (id == R.id.capture_delivery2_stock1_plus_button) {
                this.minteger1++
                this.stock1_ET!!.setText(Integer.toString(this.minteger1))
            }
        } else if (this.minteger1 > 0) {
            this.minteger1--
            this.stock1_ET!!.setText(Integer.toString(this.minteger1))
        }
    }

    companion object {
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        fun newInstance(param1: String, param2: String): CaptureDeliveryFragment {
            val fragment = CaptureDeliveryFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}
