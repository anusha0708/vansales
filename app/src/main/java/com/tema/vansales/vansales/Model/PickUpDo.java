package com.tema.vansales.vansales.Model;

import java.io.Serializable;

public class PickUpDo implements Serializable {


    public String sequenceId = "";
    public String shipmentNumber = "";
    public String city = "";
    public String customer = "";
    public String description = "";
    public String netWeight = "";
    public String volume = "";
    public Double lattitude =0.0;
    public Double longitude =0.0;
    public String packages = "";
    public String departureDate = "";
    public String departureTime = "";
    public String arrivalDate = "";
    public String arrivalTime = "";

    public String city() {
        return city;
    }
}
