package com.tema.vansales.vansales.Activitys;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.PriceAdapter;
import com.tema.vansales.vansales.Adapters.PriceListAdapter;
import com.tema.vansales.vansales.Adapters.SiteAdapter;
import com.tema.vansales.vansales.Model.PriceDO;
import com.tema.vansales.vansales.Model.SiteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.PriceListRequest;
import com.tema.vansales.vansales.Requests.SiteListRequest;

import java.util.List;

public class PriceActivity extends BaseActivity {
    private String userId,orderCode;
    private RecyclerView recycleview;
    private SiteAdapter siteAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<PriceDO> priceDOS;



    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.price_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycleview.setLayoutManager(new LinearLayoutManager(this));


        PriceDO priceDO = new PriceDO();
        showLoader();
        PriceListRequest priceListRequest = new PriceListRequest(PriceActivity.this);
        priceListRequest.setOnResultListener(new PriceListRequest.OnResultListener(){
            @Override
            public void onCompleted(boolean isError, List<PriceDO> priceDoS) {
                hideLoader();
                priceDOS =priceDoS;

                if (isError) {
                    Toast.makeText(PriceActivity.this, R.string.error_price_list, Toast.LENGTH_SHORT).show();
                } else {
                    if(priceDOS.size()>0){
                        PriceAdapter driverAdapter = new PriceAdapter(PriceActivity.this,priceDOS);
                        recycleview.setAdapter(driverAdapter);
                    }
                        else {
                        showAlert(""+R.string.error_NoData);
                    }


                }
            }
        });

        priceListRequest.execute();
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.price_list);
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        tvNoOrders  = (TextView) findViewById(R.id.tvNoOrders);

    }

}
