package com.tema.vansales.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Activitys.BaseActivity;
import com.tema.vansales.vansales.Activitys.CreateInvoiceActivity;
import com.tema.vansales.vansales.Activitys.SiteDetailsActivity;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.Model.SiteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

public class CreateInvoiceAdapter extends RecyclerView.Adapter<CreateInvoiceAdapter.MyViewHolder>  {

    private ArrayList<PickUpDo> pickUpDos;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
            tvSiteName = (TextView) view.findViewById(R.id.tvSiteName);
            tvSiteId = (TextView) view.findViewById(R.id.tvSiteId);



        }
    }


    public CreateInvoiceAdapter(Context context, ArrayList<PickUpDo> siteDOS) {
        this.context = context;
        this.pickUpDos = siteDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.create_invoice_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final PickUpDo siteDO = pickUpDos.get(position);
        holder.tvSiteName.setText(siteDO.shipmentNumber);
      //  holder.tvSiteId.setText("" + siteDO.siteId);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.SITE_NAME, String.valueOf(pickUpDos.get(pos).shipmentNumber));
                ((CreateInvoiceActivity)context).tvSelection.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.SITE_NAME, ""));
                ((CreateInvoiceActivity)context).dialog.dismiss();


            }
        });


    }

    @Override
    public int getItemCount() {
        return pickUpDos.size();
    }

}
