package com.tema.vansales.vansales.Activitys

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*

import com.tema.vansales.vansales.Adapters.ExpandableListAdapter
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.utils.MenuModel
import com.tema.vansales.vansales.utils.PreferenceUtils
import java.util.*
import android.databinding.adapters.TextViewBindingAdapter.setText
import com.tema.vansales.vansales.R.id.imageView
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import com.tema.vansales.vansales.ActionBarDrawerToggle
import com.tema.vansales.vansales.DrawerArrowDrawable


abstract class BaseActivity : AppCompatActivity() {
    public val childList = HashMap<MenuModel, List<MenuModel>> ()
    public val headerList = ArrayList<MenuModel> ()
    private var expandableListAdapter: ExpandableListAdapter? = null
    private var expandableListView: ExpandableListView? = null
    lateinit var ivMenu:ImageView
    private var lastExpandedPosition = -1
    lateinit  var toolbar: Toolbar
    lateinit var llBody: LinearLayout
    var llFooter: LinearLayout? = null

    lateinit var dialogLoader: Dialog
    private var number = ""


    lateinit  var mDrawerLayout: DrawerLayout
    private val mDrawerList: ListView? = null
    private var context: Context? = null
    private val userId: String? = null
    private val orderCode: String? = null
    lateinit  var flToolbar: FrameLayout
    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private  var drawerArrow: DrawerArrowDrawable?=null


    lateinit var preferenceUtils:PreferenceUtils
    lateinit   var ivSearch: ImageView
    private   var ivPic: ImageView? =null

//    lateinit   var ivBack: ImageView
    lateinit   var tvScreenTitle: TextView
    lateinit   var tvScreenTitleTop: TextView
    lateinit   var tvScreenTitleBottom: TextView
    lateinit   var tvDate: TextView
    lateinit   var tvDriverCode: TextView
    lateinit   var tvDriverName: TextView
    lateinit   var tvRoutingId: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.base_layout)

        context = this@BaseActivity
        //        inflater = this.getLayoutInflater();

        baseInitializeControls()
        preferenceUtils = PreferenceUtils(context)
        //        tvUsername.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.NAME, ""));
//        if (supportActionBar != null) {
//            supportActionBar!!.setDisplayShowHomeEnabled(true)
//            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
//            supportActionBar!!.setHomeButtonEnabled(true)
//            supportActionBar!!.setDisplayShowTitleEnabled(false)
//        }
//        setSupportActionBar(toolbar)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setDisplayShowTitleEnabled(false)
        }

        tvScreenTitleTop.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, ""))
        tvScreenTitleBottom.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, ""))

        //        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //        getSupportActionBar().setHomeButtonEnabled(true);
        if(preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")!=null){
            tvRoutingId.setVisibility(View.VISIBLE)
            tvDate.setVisibility(View.VISIBLE)

        }else{
            tvRoutingId.setVisibility(View.GONE)
            tvDate.setVisibility(View.GONE)
        }
        tvDate.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.CHECKIN_DATE, ""))
        tvDriverName.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, ""))
        tvDriverCode.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, ""))
        //tvDate.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, ""))
        tvRoutingId.setText(""+preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, ""))

        val image  = preferenceUtils.getStringFromPreference(PreferenceUtils.PROFILE_PICTURE, "")
        if(image.length>0){
            val decodedString =  android.util.Base64.decode(image, android.util.Base64.DEFAULT);
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            ivPic?.setImageBitmap(decodedByte)
        }
        else{
          //  ivPic?.setImageDrawable(R.drawable.usericon)
        }
//        drawerArrow = DrawerArrowDrawable(this) {
//            val isLayoutRtl: Boolean
//                get() = false
//        }

        drawerArrow = object : DrawerArrowDrawable(this) {
            override fun isLayoutRtl(): Boolean {
                return false
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        }
        toolbar.navigationIcon = drawerArrow
//        mDrawerToggle = object : ActionBarDrawerToggle(context as BaseActivity, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close, drawerArrow) {
//
//            override fun onDrawerOpened(drawerView: View) {
//                invalidateOptionsMenu()
//                super.onDrawerOpened(drawerView)
//            }
//
//            override fun onDrawerClosed(drawerView: View) {
//                invalidateOptionsMenu()
//                super.onDrawerClosed(drawerView)
//            }
//
//            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
//                super.onDrawerSlide(drawerView, slideOffset)
//            }
//
//            override fun onDrawerStateChanged(newState: Int) {
//                super.onDrawerStateChanged(newState)
//            }
//        }

        mDrawerToggle = object : ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close, drawerArrow) {

            override  fun onDrawerOpened(drawerView: View) {
                invalidateOptionsMenu()
                super.onDrawerOpened(drawerView)
            }

            override  fun onDrawerClosed(drawerView: View) {
                invalidateOptionsMenu()
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                super.onDrawerSlide(drawerView, slideOffset)
            }

            override  fun onDrawerStateChanged(newState: Int) {
                super.onDrawerStateChanged(newState)
            }
        }
        mDrawerLayout.addDrawerListener(mDrawerToggle!!)
        mDrawerToggle!!.syncState()
        toolbar.setNavigationOnClickListener {
            menuOperates()
        }
        //

        initialize()
    }
    /*} catch (JSONException e) {
            e.printStackTrace();
        }*/

     fun showLoader() {
        runOnUiThread(RunShowLoader("Loading..."))
    }

    //Method to show loader with text
    fun showLoader(msg: String) {
        runOnUiThread(RunShowLoader(msg))
    }

    fun showToast(strMsg: String) {
        if (!strMsg.isEmpty()) {
            Toast.makeText(this@BaseActivity, strMsg, Toast.LENGTH_SHORT).show()
        }
    }

      fun showAlert(strMsg: String) {
        if (!strMsg.isEmpty()) {
            showAppCompatAlert("Alert!", strMsg, "OK", "", "", true)
        }
    }

    fun showSnackbar(view: View, errorMsg: String) {

        val snackbar = Snackbar.make(view, errorMsg, Snackbar.LENGTH_SHORT)
        snackbar.show()

    }

    var alertDialog: AlertDialog.Builder? = null
    fun showAppCompatAlert(mTitle: String, mMessage: String, posButton: String, negButton: String?, from: String, isCancelable: Boolean) {
        if (alertDialog == null)
            alertDialog = AlertDialog.Builder(this@BaseActivity, R.style.AppCompatAlertDialogStyle)
        alertDialog!!.setTitle(mTitle)
        alertDialog!!.setMessage(mMessage)
        number = mMessage
        alertDialog!!.setPositiveButton(posButton) { dialog, which -> onButtonYesClick(from) }
        if (negButton != null && !negButton.equals("", ignoreCase = true))
            alertDialog!!.setNegativeButton(negButton) { dialog, which -> onButtonNoClick(from) }
        alertDialog!!.setCancelable(false)
        alertDialog!!.show()
    }

    open fun onButtonNoClick(from: String) {

    }

    open fun onButtonYesClick(from: String) {
//
//        if ("LOGOUT".equals(from, ignoreCase = true)) {
//
//            if (context !is LoginActivity) {
//             //   preferenceUtils.saveBoolean(PreferenceUtils.IS_LOGIN, false)
//                val logoutIntent = Intent(context, LoginActivity::class.java)
//                logoutIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(logoutIntent)
//                finish()
//            }
//        }

    }

    /**
     * For hiding progress dialog (Loader ).
     */

    fun hideLoader() {
        runOnUiThread {
            try {
                if (dialogLoader != null && dialogLoader.isShowing())
                    dialogLoader.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun hideKeyBoard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun setFont(group: ViewGroup, tf: Typeface) {
        val count = group.childCount
        var v: View
        for (i in 0 until count) {
            v = group.getChildAt(i)
            if (v is TextView || v is Button /* etc. */)
                (v as TextView).typeface = tf
            else if (v is ViewGroup)
                setFont(v, tf)
        }

    }

    private inner class RunShowLoader(private val strMsg: String) : Runnable {

        override fun run() {
            try {
                dialogLoader = Dialog(this@BaseActivity)
                dialogLoader.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogLoader.setCancelable(false)
                dialogLoader.getWindow()!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialogLoader.setContentView(R.layout.loader_custom)
                if (!dialogLoader.isShowing())
                    dialogLoader.show()
            } catch (e: Exception) {
               // dialogLoader = null
            }

        }
    }

    fun menuOperates() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT)
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT)
        }
    }


    protected fun disableMenuWithBackButton() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
    }

    abstract fun initialize()

    abstract fun initializeControls()

    private fun baseInitializeControls() {
        flToolbar           = findViewById<View>(R.id.flToolbar) as FrameLayout
        llBody              = findViewById<View>(R.id.llBody) as LinearLayout
        toolbar             = findViewById<View>(R.id.toolbar) as Toolbar
        expandableListView  = findViewById<View>(R.id.expandableListView) as ExpandableListView
        ivMenu              = findViewById<View>(R.id.ivMenu) as ImageView
//        ivBack              = findViewById<View>(R.id.ivBack) as ImageView
        tvScreenTitle       = findViewById<View>(R.id.tvScreenTitle) as TextView
        tvDriverName        = findViewById<View>(R.id.tvDriverName) as TextView
        tvDriverCode        = findViewById<View>(R.id.tvDriverCode) as TextView
        tvDate              = findViewById<View>(R.id.tvDate) as TextView
        tvRoutingId         = findViewById<View>(R.id.tvRoutingId) as TextView
        ivSearch            = findViewById<View>(R.id.ivSearch) as ImageView
        tvScreenTitleTop    = findViewById<View>(R.id.tvScreenTitleTop) as TextView
        tvScreenTitleBottom = findViewById<View>(R.id.tvScreenTitleBottom) as TextView
        ivPic               = findViewById<View>(R.id.ivProfilePic) as ImageView


        mDrawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        // mDrawerList          = (ListView) findViewById(R.id.left_drawer);
        prepareMenuData()
        populateExpandableList()
        ivMenu.setOnClickListener {
            val popup = PopupMenu(this@BaseActivity, ivMenu)
            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu())
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if(item.title.equals("Logout")){
//                        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT_ID)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
//                        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
                        preferenceUtils.clearPreferences()


                        // Util.storeAdminLogin(mActivity, adminCheckBox.isChecked(), Util.USER, adminUserName, adminPass)
                        val intent = Intent(this@BaseActivity, LoginActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

                        startActivity(intent)
                    }else{

                    Toast.makeText(this@BaseActivity, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show()
                    }
                    return true
                }
            })
            popup.show()//showing popup menu
        }
//        ivBack.setOnClickListener {
//           finish()
//        }

    }

    internal inner class groupClick : ExpandableListView.OnGroupClickListener {

        override fun onGroupClick(parent: ExpandableListView, v: View, groupPosition: Int, id: Long): Boolean {
            if ((this@BaseActivity.headerList.get(groupPosition) as MenuModel).isGroup && !(this@BaseActivity.headerList.get(groupPosition) as MenuModel).hasChildren) {
                this@BaseActivity.onBackPressed()

            }
            if(groupPosition==4){
                val intent = Intent(this@BaseActivity, CurrentVanSalesActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            if(groupPosition==3){
                val intent = Intent(this@BaseActivity, CreditCollectionActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
            return false
        }
    }

    /* renamed from: test.com.screensdesign.activities.BaseNavigationActivity$2 */
    internal inner class childClick : ExpandableListView.OnChildClickListener {

        override fun onChildClick(parent: ExpandableListView, v: View, groupPosition: Int, childPosition: Int, id: Long): Boolean {
            val bundle = Bundle()
            val fragment: Fragment? = null
            val existingFrag = this@BaseActivity.supportFragmentManager.findFragmentById(R.id.containerView)
            val subTitle = ""
            if (this@BaseActivity.childList.get(this@BaseActivity.headerList.get(groupPosition)) != null) {
                val model = this@BaseActivity.childList.get(this@BaseActivity.headerList.get(groupPosition))!![childPosition] as MenuModel
                if (model.menuName.equals("Vehicle Check in", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CheckinActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS or Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    startActivity(intent)
                }
                if (model.menuName.equals("Delivery", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, SalesDeliveryActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Invoice", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, SalesInvoiceActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Payments", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, SalesPaymentsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Alerts", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, AlertsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }

                if (model.menuName.equals("Cash Remit", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CashRemitActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }

                if (model.menuName.equals("Unload Van Sales Stock", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, UnloadVanSalesStockActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Inventory Count", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, InventoryCountActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Vehicle Check Out", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CheckOutActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Current Scheduled Stock", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CurrentScheduleStockActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Cash Initialization", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CashInitializationActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Load Scheduled Sales", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, LoadScheduledSales::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Delivery List", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, TabActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Load Van Sales Stock", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, LoadVanSalesActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Active Delivery", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, ActiveDeliveryActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }

                if (model.menuName.equals("Shops/Customers", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, CustomerActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Sites", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, SiteActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Products", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, ProductActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                if (model.menuName.equals("Price Lists", ignoreCase = true)) {
                    val intent = Intent(this@BaseActivity, PriceActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
                //  BaseActivity.this.changeFragment(fragment, title, subTitle);
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT)
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT)
                }
            }
            return false
        }
    }

    /* renamed from: test.com.screensdesign.activities.BaseActivity$3 */
    internal inner class groupExpand : ExpandableListView.OnGroupExpandListener {

        override fun onGroupExpand(groupPosition: Int) {
            if (!(this@BaseActivity.lastExpandedPosition == -1 || groupPosition == this@BaseActivity.lastExpandedPosition)) {
                this@BaseActivity.expandableListView!!.collapseGroup(this@BaseActivity.lastExpandedPosition)
            }
            this@BaseActivity.lastExpandedPosition = groupPosition
        }
    }

    private fun populateExpandableList() {
        this.expandableListAdapter = ExpandableListAdapter(this, this.headerList, this.childList)
        this.expandableListView!!.setAdapter(this.expandableListAdapter)
        this.expandableListView!!.setOnGroupClickListener(groupClick())
        this.expandableListView!!.setOnChildClickListener(childClick())
        this.expandableListView!!.setOnGroupExpandListener(groupExpand())
    }

    private fun prepareMenuData() {
        val childModelsList = ArrayList<MenuModel>()
        var menuModel = MenuModel("Start Day", true, true)
        this.headerList.add(menuModel)
        childModelsList.add(MenuModel("Vehicle Check in", false, false))
        childModelsList.add(MenuModel("Load Van Sales Stock", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList)
        }
        var childModelsList2 = ArrayList<MenuModel>()
        menuModel = MenuModel("Scheduled Sales", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Delivery List", false, false))
        childModelsList2.add(MenuModel("Active Delivery", false, false))
        childModelsList2.add(MenuModel("Alerts", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Spot Sales", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Delivery", false, false))
        childModelsList2.add(MenuModel("Invoice", false, false))
        childModelsList2.add(MenuModel("Payments", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Credit Collection", true, true)
        this.headerList.add(menuModel)
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Current Van Sales Stock", true, true)
        this.headerList.add(menuModel)
//        childModelsList2.add(MenuModel("Current Van Sales Stock", false, false))
//        childModelsList2.add(MenuModel("Current Scheduled Stock", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }

        childModelsList2 = ArrayList()
        menuModel = MenuModel("End Day", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Cash Remit", false, false))
        childModelsList2.add(MenuModel("Unload Van Sales Stock", false, false))
        childModelsList2.add(MenuModel("Inventory Count", false, false))
        childModelsList2.add(MenuModel("Vehicle Check Out", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
        childModelsList2 = ArrayList()
        menuModel = MenuModel("Master Data", true, true)
        this.headerList.add(menuModel)
        childModelsList2.add(MenuModel("Sites", false, false))
        childModelsList2.add(MenuModel("Shops/Customers", false, false))
        childModelsList2.add(MenuModel("Products", false, false))
        childModelsList2.add(MenuModel("Price Lists", false, false))
        if (menuModel.hasChildren) {
            this.childList.put(menuModel, childModelsList2)
        }
    }

}
