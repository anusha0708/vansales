package com.tema.vansales.vansales.Activitys

import android.support.design.widget.TabLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout


import com.tema.vansales.vansales.R

class TabActivity : BaseActivity() {
    private var rlPartyActivity: RelativeLayout? = null
    lateinit var adapter: PagerAdapter
    lateinit var viewPager: ViewPager
    lateinit var tabLayout: TabLayout

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.activity_tab, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tabLayout = findViewById<View>(R.id.tab_layout) as TabLayout
        tabLayout.addTab(tabLayout.newTab().setText(R.string.list))
        tabLayout.addTab(tabLayout.newTab().setText(R.string.map))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        viewPager = findViewById<View>(R.id.pager) as ViewPager

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        adapter = com.tema.vansales.vansales.Adapters.PagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter

    }

    override fun initializeControls() {
        tvScreenTitle.setText("Delivery List");
        ivSearch.visibility = View.VISIBLE
        //ivBack.visibility = View.GONE
//        tvScreenTitleTop.visibility = View.VISIBLE
//        tvScreenTitleBottom.visibility = View.VISIBLE

    }


}