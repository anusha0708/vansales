package com.tema.vansales.vansales.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Activitys.VehicleRoutingList;
import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Adapters.VehicleRouteAdapter;
import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.Model.RouteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandy on 2/15/2018.
 */

public class RouteListFragmment extends Fragment  {
    private String userId,orderCode;
    private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private CustomerAdapter orderHistoryAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<RouteDO> customerDos;

    ArrayList<PickUpDo> pickUpDos;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.vehicle_screen, container, false);

        // userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");
        recycleview = (RecyclerView)rootView. findViewById(R.id.recycleview);
        tvNoOrders  = (TextView) rootView.findViewById(R.id.tvNoOrders);
        CustomerDo orderHistoryDo = new CustomerDo();
        preferenceUtils = new PreferenceUtils(getActivity());

        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
           if(id.length()>0){
               recycleview.setVisibility(View.VISIBLE);
               tvNoOrders.setVisibility(View.GONE);
               DisplayPickupListRequest driverListRequest = new DisplayPickupListRequest(getActivity(), id);
               driverListRequest.setOnResultListener(new DisplayPickupListRequest.OnResultListener() {
                   @Override
                   public void onCompleted(boolean isError, ArrayList<PickUpDo> pods) {
                       pickUpDos = pods;
                       if (isError) {
                           Toast.makeText(getActivity(), "Failed to get route list", Toast.LENGTH_SHORT).show();
                       } else {

                           VehicleRouteAdapter driverAdapter = new VehicleRouteAdapter(getActivity(), pods);
                           recycleview.setLayoutManager(new LinearLayoutManager(getActivity()));

                           recycleview.setAdapter(driverAdapter);

                       }


                   }
               });
               driverListRequest.execute();
           }else {
               recycleview.setVisibility(View.GONE);
               tvNoOrders.setVisibility(View.VISIBLE);
           }


        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);


        return rootView;
    }
}