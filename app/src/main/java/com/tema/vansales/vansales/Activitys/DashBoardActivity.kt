package com.tema.vansales.vansales.Activitys

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.R


class DashBoardActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.dashboard, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.dash_board)
        //ivBack.visibility = View.GONE
        ivMenu.visibility = View.VISIBLE

    }

}