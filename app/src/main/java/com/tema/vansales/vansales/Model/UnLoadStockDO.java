package com.tema.vansales.vansales.Model;

import java.io.Serializable;

public class UnLoadStockDO implements Serializable {

    public String productDescription = "";
    public String productName = "";
    public String weight = "";
    public String salesUnit = "";
    public String quantity = "";
    public String maxQuantity = "";
    public String number = "";
    public String customer = "";
    public String product = "";
    public int itemCount = 0;


}
