package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.support.v4.widget.DrawerLayout
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*

import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.Requests.ConfigRequest
import com.tema.vansales.vansales.utils.CustomProgressDialog
import com.tema.vansales.vansales.utils.PreferenceUtils

class ConfigurationActivity : BaseActivity() {
    lateinit var llSplash: RelativeLayout
    private var saveBtn: Button? = null
    private var resetBtn: Button? = null
    private var ipEditText: EditText? = null
    private var portEditText: EditText? = null
    private var poolEditText: EditText? = null
    private var userNameEditText: EditText? = null
    private var passEditText: EditText? = null
    private var backButton: ImageView? = null
    internal lateinit var ip: String
    internal lateinit var port: String
    internal lateinit var pool: String
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String

    internal lateinit var pip: String
    internal lateinit var pport: String
    internal lateinit var ppool: String
    internal lateinit var pdbUser: String
    internal lateinit var pdbPass: String

    internal var customProgressDialog: CustomProgressDialog? = null

    override fun initializeControls() {
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)

        val container = findViewById<View>(R.id.configcont)
        //  CustomBranding.setCustomBG(container);
        preferenceUtils = PreferenceUtils(this@ConfigurationActivity)

        backButton       = findViewById<View>(R.id.ivBackC) as ImageView
        resetBtn         = findViewById<View>(R.id.btnReset) as Button
        saveBtn          = findViewById<View>(R.id.btnSave) as Button
        ipEditText       = findViewById<View>(R.id.etIpAddress) as EditText
        portEditText     = findViewById<View>(R.id.etPortNumber) as EditText
        poolEditText     = findViewById<View>(R.id.etAlias) as EditText
        userNameEditText = findViewById<View>(R.id.etUserName) as EditText
        passEditText     = findViewById<View>(R.id.etPassword) as EditText
//
        ipEditText!!.setText("183.82.9.23")
        portEditText!!.setText("8124")
        poolEditText!!.setText("VANSALEDEV")
        userNameEditText!!.setText("RENUKA")
        passEditText!!.setText("*U024@T3m@")


        backButton!!.setOnClickListener { performBack() }

        resetBtn!!.setOnClickListener {
            ipEditText!!.setText("")
            portEditText!!.setText("")
            poolEditText!!.setText("")
            userNameEditText!!.setText("")
            passEditText!!.setText("")
        }
        ip = ipEditText!!.text.toString()
        port = portEditText!!.text.toString()
        pool = poolEditText!!.text.toString()
        dbUser = userNameEditText!!.text.toString()
        dbPass = passEditText!!.text.toString()
        saveBtn!!.setOnClickListener(OnClickListener {
            ip = ipEditText!!.text.toString().trim { it <= ' ' }
            if (ip.length == 0) {
                showAlert("Please enter IP")
                return@OnClickListener
            }

            port = portEditText!!.text.toString().trim { it <= ' ' }
            if (port.length == 0) {
                showAlert("Please enter Port")
                return@OnClickListener
            }

            pool = poolEditText!!.text.toString().trim { it <= ' ' }
            if (pool.length == 0) {
                showAlert("Please enter Pool")
                return@OnClickListener
            }

            dbUser = userNameEditText!!.text.toString().trim { it <= ' ' }
            if (dbUser.length == 0) {
                showAlert("Please enter User name")
                return@OnClickListener
            }

            dbPass = passEditText!!.text.toString().trim { it <= ' ' }
            if (dbPass.length == 0) {
                showAlert("Please enter password")
                return@OnClickListener
            }


            //                if (!ip.endsWith("/")) {
            //                    ip = ip + "/";
            //                }
            customProgressDialog = CustomProgressDialog(this@ConfigurationActivity, "Please wait...")
            customProgressDialog!!.show()
            val configRequest = ConfigRequest()
            configRequest.setOnResultListener { isError, isSuccess ->
                println("isError $isError isSuccess $isSuccess")
                if (customProgressDialog != null && customProgressDialog!!.isShowing) {
                    customProgressDialog!!.dismiss()
                }
                if (isError) {
                    showAppCompatAlert("Alert!", "Please Enter Valid Credentials", "OK", "", "FAILURE", false)
                } else {
                    if (isSuccess) {
                        preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, ip)
                        preferenceUtils.saveString(PreferenceUtils.PORT, port)
                        preferenceUtils.saveString(PreferenceUtils.ALIAS, pool)
                        preferenceUtils.saveString(PreferenceUtils.USER_NAME, dbUser)
                        preferenceUtils.saveString(PreferenceUtils.PASSWORD, dbPass)

                        showAppCompatAlert("Success", "Configured Successfully", "OK", "", "SUCCESS", false)

                        // Util.storeAppUrl(ConfigurationActivity.this, ip, port, pool, dbUser, dbPass);

                    } else {
                        showAppCompatAlert("Alert!", "Please Enter Valid Credentials", "OK", "", "FAILURE", false)


                    }
                }
            }
            configRequest.execute(ip, port, pool, dbUser, dbPass)
        })

    }

    override fun initialize() {
        llSplash = layoutInflater.inflate(R.layout.fragment_configuration, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        tvScreenTitle.setText("Configuration")


//        ipEditText!!.setText("183.82.9.23")
//        portEditText!!.setText("8124")
//        poolEditText!!.setText("VANSALEDEV")
//        userNameEditText!!.setText("RENUKA")
//        passEditText!!.setText("*U024@T3m@")

    }


    override fun onButtonNoClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            finish()

        }
    }

    override fun onButtonYesClick(from: String) {

        if ("SUCCESS".equals(from, ignoreCase = true)) {
            val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()

        } else
            if ("FAILURE".equals(from, ignoreCase = true)) {


                //finish()
            }

    }

    override fun onBackPressed() {
        performBack()
    }

    private fun performBack() {
        val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }


}
