package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.Adapters.InvoiceAdapter
import com.tema.vansales.vansales.Adapters.PaymentAdapter
import com.tema.vansales.vansales.Model.LoadStockDO
import com.tema.vansales.vansales.R
import java.util.ArrayList


class SalesPaymentsActivity : BaseActivity() {
    lateinit var invoiceAdapter: PaymentAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview :RecyclerView
    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.framgment_sales_payments_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.sales_payments)
        recycleview = findViewById(R.id.recycleview) as RecyclerView
        val linearLayoutManager = LinearLayoutManager(this)

        recycleview.setLayoutManager(linearLayoutManager)
        invoiceAdapter = PaymentAdapter(this@SalesPaymentsActivity,  null)
        recycleview.setAdapter(invoiceAdapter)
        val btnCreate = findViewById(R.id.btnCreate) as Button

        btnCreate.setOnClickListener {
            val intent = Intent(this@SalesPaymentsActivity, CreatePaymentActivity::class.java);
            startActivity(intent);
        }
    }

}