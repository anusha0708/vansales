package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Adapters.VehicleRouteAdapter;
import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.Model.PickUpDo;
import com.tema.vansales.vansales.Model.RouteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.CustomerListRequest;
import com.tema.vansales.vansales.Requests.DisplayPickupListRequest;
import com.tema.vansales.vansales.Requests.VehicleRouteListRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

public class VehicleRoutingList extends BaseActivity implements LocationListener {
    private String userId,orderCode;
          private PreferenceUtils preferenceUtils;
    private RecyclerView recycleview;
    private CustomerAdapter orderHistoryAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<RouteDO> customerDos;

    ArrayList<PickUpDo> pickUpDos;


    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.vehicle_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        preferenceUtils = new PreferenceUtils(VehicleRoutingList.this);
        // userId = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_ID, "");

        CustomerDo orderHistoryDo = new CustomerDo();
        DisplayPickupListRequest driverListRequest = new DisplayPickupListRequest(VehicleRoutingList.this, "VR-180313-FR011-115");

        driverListRequest.setOnResultListener(new DisplayPickupListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, ArrayList<PickUpDo> pods) {
                hideLoader();
                pickUpDos = pods;
                if (isError) {
                    Toast.makeText(VehicleRoutingList.this, R.string.error_route_list, Toast.LENGTH_SHORT).show();
                } else {


                    VehicleRouteAdapter driverAdapter = new VehicleRouteAdapter(VehicleRoutingList.this, pods);
                    recycleview.setLayoutManager(new LinearLayoutManager(VehicleRoutingList.this));

                    recycleview.setAdapter(driverAdapter);

                }


            }
        });
        driverListRequest.execute();
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    @Override
    public void initializeControls() {
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        tvNoOrders  = (TextView) findViewById(R.id.tvNoOrders);
        tvScreenTitle.setText(R.string.route_list);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VehicleRoutingList.this,CurrentMapActivity.class);
                startActivity(intent);
            }
        });
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

    }

    @Override
    public void onLocationChanged(Location location) {
        double latti = location.getLatitude();
        double longi = location.getLongitude();

        preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE2,latti);
        preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE2,longi);

    }

// Register the listener with the Location Manager to receive location updates}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
