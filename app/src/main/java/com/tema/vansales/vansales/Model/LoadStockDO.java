package com.tema.vansales.vansales.Model;

import java.io.Serializable;

public class LoadStockDO implements Serializable {

    public String product            = "";
    public String productDescription = "";
    public String stockUnit          = "";
    public String weightUnit         = "";
    public String productWeight      = "";
    public String productVolume      = "";
    public String totalStockUnit     = "";
    public String totalStockVolume   = "";
    public String quantity           = "";
    public int itemCount           = 0;


}
