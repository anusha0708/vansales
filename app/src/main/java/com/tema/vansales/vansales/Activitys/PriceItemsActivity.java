package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Adapters.PriceAdapter;
import com.tema.vansales.vansales.Adapters.PriceItemAdapter;
import com.tema.vansales.vansales.Adapters.SiteAdapter;
import com.tema.vansales.vansales.Model.CustomerDetailsDo;
import com.tema.vansales.vansales.Model.PriceDO;
import com.tema.vansales.vansales.Model.PriceItemDO;
import com.tema.vansales.vansales.Model.PriceItemMainDO;
import com.tema.vansales.vansales.Model.SiteDetailsDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.PriceITEMRequest;
import com.tema.vansales.vansales.Requests.PriceItemListRequest;
import com.tema.vansales.vansales.Requests.PriceListRequest;

import java.util.ArrayList;
import java.util.List;

public class PriceItemsActivity extends BaseActivity {
    private String userId,Code;
    private RecyclerView recycleview;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    ArrayList<PriceItemDO> priceItemDOS;
    LinearLayout ll1;
    ListView listView;


    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.price_item_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getIntent().hasExtra("Code")) {
            Code = getIntent().getExtras().getString("Code");
        }
        recycleview.setLayoutManager(new LinearLayoutManager(this));

        listView = (ListView) findViewById(R.id.lvItems);

        PriceDO priceDO = new PriceDO();
        showLoader();
        PriceITEMRequest priceListRequest = new PriceITEMRequest(Code,PriceItemsActivity.this);
        priceListRequest.setOnResultListener(new PriceITEMRequest.OnResultListener(){
            @Override
            public void onCompleted(boolean isError, PriceItemMainDO priceDOS) {
                hideLoader();
                priceDOS =priceDOS;
                if (isError) {
                    Toast.makeText(PriceItemsActivity.this, "Failed to get price list", Toast.LENGTH_SHORT).show();
                } else {
                    priceItemDOS = priceDOS.priceItemDOS;

                          if(priceItemDOS.size()>0){
                                      ll1.setVisibility(View.VISIBLE);
                                  recycleview.setVisibility(View.VISIBLE);
                                  tvNoOrders.setVisibility(View.GONE);
                              PriceItemAdapter driverAdapter = new PriceItemAdapter(PriceItemsActivity.this,priceItemDOS);
                              recycleview.setAdapter(driverAdapter);
//                              showAppCompatAlert("Alert!","No Records", "OK", "", "FAILURE", false);
                          }else {
                              recycleview.setVisibility(View.GONE);
                              tvNoOrders.setVisibility(View.VISIBLE);
                              ll1.setVisibility(View.GONE);
                          }

//                    CustomAdapter customAdapter = new CustomAdapter(PriceItemsActivity.this, priceDOS.priceItemDOS);
//                    // Set a adapter object to the listview
//                    listView.setAdapter(customAdapter);
                }
            }
        });

        priceListRequest.execute();
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    class CustomAdapter extends BaseAdapter {
        public Context context;
        ArrayList<PriceItemDO> siteDetailsDos;


        public CustomAdapter(Context context, ArrayList<PriceItemDO> customerDetailsDos) {

            this.context = context;
            this.siteDetailsDos = customerDetailsDos;
        }

        @Override
        public int getCount() {
            // return the all apps count
            if (siteDetailsDos.size() > 0)
                return siteDetailsDos.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflating installedapps_customlayout
            View view = getLayoutInflater().inflate(R.layout.price_item_data, null);
                  if(siteDetailsDos.size()>0){
                      TextView addressLine = (TextView) view.findViewById(R.id.tvPriceId);
                      TextView addressDescription = (TextView) view.findViewById(R.id.tvPriceName);
                      TextView postalCode = (TextView) view.findViewById(R.id.tvPriceValue);
                      ;
                      addressLine.setText("" + siteDetailsDos.get(position).record);
                      addressDescription.setText("" + siteDetailsDos.get(position).startDate);
                      postalCode.setText("" + siteDetailsDos.get(position).endDate);
                  }else {
                      showAlert("NO Records Found");
                  }


            return view;
        }

    }
    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.price_list);
        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        ll1 = (LinearLayout) findViewById(R.id.ll1);

        tvNoOrders  = (TextView) findViewById(R.id.tvNoOrders);

    }

}
