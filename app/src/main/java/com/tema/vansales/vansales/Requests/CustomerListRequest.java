package com.tema.vansales.vansales.Requests;

import android.content.Context;
import android.os.AsyncTask;
import android.print.PrintManager;


import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vijay Dhas on 26/05/16.
 */
public class CustomerListRequest extends AsyncTask<String, Void, Boolean> {
    List<CustomerDo> customers;
    PreferenceUtils preferenceUtils;
    String pdbUser,pdbPass;
    Context context;
    String username,password,ip,pool,port;

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, List<CustomerDo> customers);

    }
    public CustomerListRequest(Context context) {
        this.context = context;
    }

    public boolean runRequest() {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "query";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(context);

        username   = preferenceUtils.getStringFromPreference(PreferenceUtils.USER_NAME, "");
        password   = preferenceUtils.getStringFromPreference(PreferenceUtils.PASSWORD, "");
        ip         = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port       = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool       = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://"+ip+":"+port+"/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", "XX10CBPC");
        request.addProperty("listSize", 9999);

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params

        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser",username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username+ ":" +password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString "+xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            CustomerDo customer = null;
            customers = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

//
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        customer = new CustomerDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("BPCNUM")) {
                            customer.customerId(text);
                        } else if (attribute.equalsIgnoreCase("BPCNAM")) {
                            customer.customerName(text);
                        }
                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        customers.add(customer);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customers);
        }
    }
}