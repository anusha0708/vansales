package com.tema.vansales.vansales.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarUtils
{
	

	public static final String DATE_STD_PATTERN_PRINT    = "yyyy-MM-dd";//2018-01-06

	public static String getCurrentDate(){

		String formattedDate = "";
		try{
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat(DATE_STD_PATTERN_PRINT);
//            formattedDate = df.format(c.getTime()+" "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE));

            formattedDate = df.format(c.getTime())+"   "+c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE);
		}catch (Exception e){
			e.printStackTrace();
		}
		return formattedDate;
	}
}



