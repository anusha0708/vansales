package com.tema.vansales.vansales.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PreferenceUtils {

    private SharedPreferences preferences;
    private SharedPreferences.Editor edit;
    public static  String STOCK_COUNT = "STOCK_COUNT";
    public static  String CUSTOMER = "CUSTOMER";
    public static  String SITE_NAME = "SITE_NAME";
    public static  String SHIPMENT_ID = "SHIPMENT_ID";
    public static String USER_NAME = "user_name";
    public static String PASSWORD = "password";
    public static String IP_ADDRESS = "address";
    public static String PORT = "port";
    public static String ALIAS = "alias";
    public static String VEHICLE_ROUTE_ID = "VEHICLE_ROUTE_ID";
    public static String PROFILE_PICTURE = "PROFILE_PICTURE";
    public static  String VEHICLE_CHECKED_ROUTE_ID = "VEHICLE_CHECKED_ROUTE_ID";
    public static String CARRIER_ID = "VEHICLE_ROUTE_ID";
    public static  String SHIPMENT_DT = "SHIPMENT_DT";
    public static  String SHIPMENT_AT = "SHIPMENT_AT";
    public static String EM_CARRIER_ID = "EM_CARRIER_ID";

    public static  String EMAIL = "EMAIL";
    public static  String PHONE = "PHONE";
    public static  String LONGITUDE2 ="LONG2" ;
    public static  String LATTITUDE2= "LAT2";
    public static  String LONGITUDE ="LONG" ;
    public static  String LATTITUDE = "LAT";
    public static  String CHECKIN_DATE = "CHECKIN_DATE";
    public static String DRIVER_ID = "DRIVER_ID";

    public static String INVOICE_ID = "INVOICE_ID";
    public static String PAYMENT_ID = "PAYMENT_ID";

    public PreferenceUtils(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        edit = preferences.edit();
    }
    public void clearPreferences()
    {
        edit.clear().apply();
        edit.commit();
    }

    public void saveString(String strKey, String strValue) {
        edit.putString(strKey, strValue);
        edit.commit();
    }

    public void saveInt(String strKey, int value) {
        edit.putInt(strKey, value);
        edit.commit();
    }


    public void saveLong(String strKey, Long value) {
        edit.putLong(strKey, value);
        edit.commit();
    }

    public void saveFloat(String strKey, float value) {
        edit.putFloat(strKey, value);
        edit.commit();
    }

    public void saveDouble(String strKey, Double value) {
        edit.putString(strKey, "" + value);
        edit.commit();
    }

    public void saveBoolean(String strKey, boolean value) {
        edit.putBoolean(strKey, value);
        edit.commit();
    }

    public void removeFromPreference(String strKey) {
        edit.remove(strKey);
    }

    public String getStringFromPreference(String strKey, String defaultValue) {
        return preferences.getString(strKey, defaultValue);
    }

    public boolean getbooleanFromPreference(String strKey, boolean defaultValue) {
        return preferences.getBoolean(strKey, defaultValue);
    }

    public int getIntFromPreference(String strKey, int defaultValue) {
        return preferences.getInt(strKey, defaultValue);
    }

    public long getLongFromPreference(String strKey) {
        return preferences.getLong(strKey, 0);
    }

    public float getFloatFromPreference(String strKey, float defaultValue) {
        return preferences.getFloat(strKey, defaultValue);
    }

    public double getDoubleFromPreference(String strKey, double defaultValue) {
        return Double.parseDouble(preferences.getString(strKey, "" + defaultValue));
    }
}
