package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tema.vansales.vansales.R

//
class PaymentsActivity : BaseActivity() {

    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.payments, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.payments)

        var btnInvoice = findViewById(R.id.btnInvoice) as Button
        btnInvoice.setOnClickListener {
            var intent = Intent(this@PaymentsActivity,InvoicesActivity::class.java)
            startActivity(intent)
        }


        var btnPayments = findViewById(R.id.btnPayments) as Button
        btnPayments.setOnClickListener {
            var intent = Intent(this@PaymentsActivity,ListPaymentsActivity::class.java)
            startActivity(intent)
        }


    }
}