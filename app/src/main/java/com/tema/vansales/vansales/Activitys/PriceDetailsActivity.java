package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.PriceListAdapter;
import com.tema.vansales.vansales.Adapters.ProductAdapter;
import com.tema.vansales.vansales.Model.CustomerDetailsDo;
import com.tema.vansales.vansales.Model.PriceDetailMainDO;
import com.tema.vansales.vansales.Model.PriceDetailsDO;
import com.tema.vansales.vansales.Model.ProductDetailMainDO;
import com.tema.vansales.vansales.Model.ProductDetailsDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.PriceDetailsRequest;
import com.tema.vansales.vansales.Requests.ProductDetailsRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class PriceDetailsActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private HorizontalScrollView llOrderDetails;
    private TextView tvName;
    private TextView tvDescription;

    ArrayList<PriceDetailsDO> priceDetailsDOS;
    private TextView currency;
    private TextView rule;
    private TextView term;
    private TextView category;
    private TextView address;
    private TextView bill;
    private TextView payCustmer;
    private TextView code;
    private TextView payByAddress;
    private TextView billAddress;

    PreferenceUtils preferenceUtils;
    String priceCode, priceList, startDate, endDate;
    ListView listView;
    // CustomAdapter customAdapter;
    RecyclerView recyclerView;
    // private ActivityMainBinding binding;

    @Override
    public void initialize() {
        //    binding = DataBindingUtil.setContentView(this, R.layout.price_details);

        llOrderDetails = (HorizontalScrollView) getLayoutInflater().inflate(R.layout.price_details, null);
        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // llshoppingCartLayout.setVisibility(View.GONE);

        if (getIntent().hasExtra("Record")) {
            priceCode = getIntent().getExtras().getString("Record");
        }
        if (getIntent().hasExtra("priceList")) {
            priceList = getIntent().getExtras().getString("priceList");
        }
        if (getIntent().hasExtra("startDate")) {
            startDate = getIntent().getExtras().getString("startDate");
        }
        if (getIntent().hasExtra("endDate")) {
            endDate = getIntent().getExtras().getString("endDate");
        }
       // listView = (ListView) findViewById(R.id.lvItems);
        recyclerView = (RecyclerView) findViewById(R.id.recycleview);

        preferenceUtils = new PreferenceUtils(PriceDetailsActivity.this);
        PriceDetailsRequest driverListRequest = new PriceDetailsRequest(priceCode, priceList, startDate, endDate, PriceDetailsActivity.this);
        driverListRequest.setOnResultListener(new PriceDetailsRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, PriceDetailMainDO priceDetailMainDO) {
                if (isError) {
                    Toast.makeText(PriceDetailsActivity.this, "Failed to get details list", Toast.LENGTH_SHORT).show();
                } else {
                    priceDetailsDOS = priceDetailMainDO.priceDetailsDOS;
                    if(priceDetailsDOS.size()>0){
                        GridLayoutManager layoutManager =
                                new GridLayoutManager(PriceDetailsActivity.this, priceDetailsDOS.size(), GridLayoutManager.HORIZONTAL, false);
                        recyclerView.setLayoutManager(new LinearLayoutManager(PriceDetailsActivity.this));
                        PriceListAdapter driverAdapter = new PriceListAdapter(PriceDetailsActivity.this,priceDetailsDOS);
                        recyclerView.setAdapter(driverAdapter);
                    }else {
                        showAlert(""+R.string.error_NoData);
                    }

//                    tvName.setText("" + priceDetailMainDO.customer);
//                    tvDescription.setText("" + priceDetailMainDO.product);
//                    currency.setText("" + priceDetailMainDO.id);
//                    rule.setText("" + priceDetailMainDO.customer);




                }

            }
        });
        //listView.setOnItemClickListener(this);

        driverListRequest.execute();
    }



    @Override
    public void initializeControls() {
        tvScreenTitle.setText("Price Details");

//        tvName = (TextView) findViewById(R.id.tvPriceCode);
//        tvDescription = (TextView) findViewById(R.id.tvPriceList);
//        currency = (TextView) findViewById(R.id.tvStartDate);
//        rule = (TextView) findViewById(R.id.tvEndDate);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
