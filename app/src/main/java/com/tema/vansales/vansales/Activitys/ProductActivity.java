package com.tema.vansales.vansales.Activitys;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.PriceAdapter;
import com.tema.vansales.vansales.Adapters.ProductAdapter;
import com.tema.vansales.vansales.Adapters.SiteAdapter;
import com.tema.vansales.vansales.Model.ProductDO;
import com.tema.vansales.vansales.Model.SiteDO;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.ProductListRequest;
import com.tema.vansales.vansales.Requests.SiteListRequest;

import java.util.List;

public class ProductActivity extends BaseActivity {
    private String userId,orderCode;
    private RecyclerView recycleview;
    private SiteAdapter siteAdapter;
    private RelativeLayout llOrderHistory;
    private TextView tvNoOrders;
    private List<ProductDO> productDOS;

    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.site_screen, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recycleview.setLayoutManager(new LinearLayoutManager(this));


        SiteDO siteDO = new SiteDO();
        ProductListRequest siteListRequest = new ProductListRequest(ProductActivity.this);
        showLoader();
        siteListRequest.setOnResultListener(new ProductListRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, List<ProductDO> productDoS) {
                hideLoader();
                productDOS =productDoS;
                if (isError) {
                    Toast.makeText(ProductActivity.this, R.string.error_product_list, Toast.LENGTH_SHORT).show();
                } else {
                    if(productDOS.size()>0){
                        ProductAdapter driverAdapter = new ProductAdapter(ProductActivity.this,productDOS);
                        recycleview.setAdapter(driverAdapter);
                    }
                    else {
                        showAlert(""+R.string.error_NoData);
                    }


                }
            }
        });

        siteListRequest.execute();
        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText(R.string.product_list);

        recycleview = (RecyclerView) findViewById(R.id.recycleview);
        tvNoOrders  = (TextView) findViewById(R.id.tvNoOrders);

    }

}
