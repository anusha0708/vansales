package com.tema.vansales.vansales.Activitys;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.tema.vansales.vansales.Adapters.CustomerAdapter;
import com.tema.vansales.vansales.Model.CustomerDetailsDo;
import com.tema.vansales.vansales.Model.CustomerDetailsMainDo;
import com.tema.vansales.vansales.Model.CustomerDo;
import com.tema.vansales.vansales.R;
import com.tema.vansales.vansales.Requests.CustomerDetailsRequest;
import com.tema.vansales.vansales.Requests.CustomerListRequest;
import com.tema.vansales.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomerDetailsActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private ScrollView llOrderDetails;
    private TextView tvName;
    private TextView tvDescription;

    ArrayList<CustomerDetailsDo> customerDos;
    private TextView currency;
    private TextView rule;
    private TextView term;
    private TextView category;
    private TextView address;
    private TextView bill;
    private TextView payCustmer;
    private TextView code;
    private TextView payByAddress;
    private TextView billAddress;

    PreferenceUtils preferenceUtils;
    String orderCode;
    ListView listView;
    CustomAdapter customAdapter;
    @Override
    public void initialize() {
        llOrderDetails = (ScrollView) getLayoutInflater().inflate(R.layout.customer_details, null);
        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // llshoppingCartLayout.setVisibility(View.GONE);

        if (getIntent().hasExtra("Code")) {
            orderCode = getIntent().getExtras().getString("Code");
        }
        listView = (ListView) findViewById(R.id.lvItems);


        preferenceUtils = new PreferenceUtils(CustomerDetailsActivity.this);
        CustomerDetailsRequest driverListRequest = new CustomerDetailsRequest(orderCode, CustomerDetailsActivity.this);
        driverListRequest.setOnResultListener(new CustomerDetailsRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, CustomerDetailsMainDo customerDetailsMainDo) {
                if (isError) {
                    Toast.makeText(CustomerDetailsActivity.this, R.string.error_details_list, Toast.LENGTH_SHORT).show();
                } else {
                    customerDos = customerDetailsMainDo.customerDetailsDos;

                    tvName.setText("" + customerDetailsMainDo.name);
                    tvDescription.setText("" + customerDetailsMainDo.descrption);
                    currency.setText("" + customerDetailsMainDo.currency);
                    rule.setText("" + customerDetailsMainDo.rule);
                    term.setText("" + customerDetailsMainDo.term);
                    address.setText("" + customerDetailsMainDo.address);
                    bill.setText("" + customerDetailsMainDo.bill);
                    payCustmer.setText("" + customerDetailsMainDo.pay);
                    code.setText(" " + customerDetailsMainDo.code);
                    billAddress.setText("" + customerDetailsMainDo.billAddress);
                    payByAddress.setText(" " + customerDetailsMainDo.payByAddress);
//                    if (customerDos.size() > 0) {
//                        for (int k = 0; k < customerDos.size(); k++) {
//
//
//
////                    CustomerAdapter driverAdapter = new CustomerAdapter(customerDos);
////                    recycleview.setAdapter(driverAdapter);
//                        }
//                    }}
                     customAdapter = new CustomAdapter(CustomerDetailsActivity.this, customerDos);

                    // Set a adapter object to the listview
                    listView.setAdapter(customAdapter);

                }

            }
        });
        listView.setOnItemClickListener(this);

        driverListRequest.execute();
    }

    class CustomAdapter extends BaseAdapter {
        public Context context;
        ArrayList<CustomerDetailsDo> listOrderHistoryDetailsDO;


        public CustomAdapter(Context context, ArrayList<CustomerDetailsDo> customerDetailsDos) {

            this.context = context;
            this.listOrderHistoryDetailsDO = customerDetailsDos;
        }

        @Override
        public int getCount() {
            // return the all apps count
            if (customerDos.size() > 0)
                return customerDos.size();
            else
                return 0;
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Inflating installedapps_customlayout
            View view = getLayoutInflater().inflate(R.layout.details_list, null);

            TextView addressLine = (TextView) view.findViewById(R.id.addressLine);
            TextView addressDescription = (TextView) view.findViewById(R.id.addressDescription);
            TextView postalCode         = (TextView) view.findViewById(R.id.postalCode);
            TextView city = (TextView) view.findViewById(R.id.city);
            TextView telephone = (TextView) view.findViewById(R.id.telephone);
            TextView mobile = (TextView) view.findViewById(R.id.mobile);

            addressLine.setText("" + customerDos.get(position).addressLine);
            //   packageNameTextView.setText(customerDos.get(position));
            addressDescription.setText("" + customerDos.get(position).addressDescription);
            postalCode.setText("" + customerDos.get(position).postalCode);
            city.setText("" + customerDos.get(position).city);
            telephone.setText("" + customerDos.get(position).telephone);
            mobile.setText("" + customerDos.get(position).mobile);

            return view;
        }

    }

    @Override
    public void initializeControls() {
        tvScreenTitle.setText("Customer Details");

        tvName        = (TextView) findViewById(R.id.tvItemName);
        tvDescription = (TextView) findViewById(R.id.tvItemDescription);
        currency      = (TextView) findViewById(R.id.currency);
        rule          = (TextView) findViewById(R.id.rule);
        term          = (TextView) findViewById(R.id.term);
        address       = (TextView) findViewById(R.id.address);
        bill          = (TextView) findViewById(R.id.bill);
        payCustmer    = (TextView) findViewById(R.id.payCustmer);
        code          = (TextView) findViewById(R.id.code);
        billAddress   = (TextView) findViewById(R.id.billAddress);

        payByAddress = (TextView) findViewById(R.id.payByAddress);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
