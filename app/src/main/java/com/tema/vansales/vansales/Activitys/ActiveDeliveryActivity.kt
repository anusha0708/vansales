package com.tema.vansales.vansales.Activitys

import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.tema.vansales.vansales.R
import com.tema.vansales.vansales.utils.LogUtils
import com.tema.vansales.vansales.utils.PreferenceUtils

//
class ActiveDeliveryActivity : BaseActivity(), OnMapReadyCallback {
    lateinit var googleMap: GoogleMap
    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0!!
       // googleMap.setMyLocationEnabled(true)
        if (googleMap == null) {
            Toast.makeText(applicationContext, R.string.map_error, Toast.LENGTH_SHORT).show()
        } else {
            googleMap.getUiSettings().setZoomControlsEnabled(true)

        }
        try {
            showLocation()

        } catch (e: Exception) {

        }


    }

    private fun showLocation() {
        try {

            val markerOptions = MarkerOptions()
            preferenceUtils = PreferenceUtils(this@ActiveDeliveryActivity)
            val lat  = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0)
            val lng  = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0)

            // Setting latitude and longitude for the marker
            markerOptions.position(LatLng(lat,lng))


            // Adding marker on the Google Map
            googleMap.addMarker(markerOptions)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 8f))

        } catch (e: Exception) {
           // Log.e(TAG, R.string.fail_location.toString(), e)
        }

    }


    override  protected fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = getLayoutInflater().inflate(R.layout.fragment_active_delivery, null) as ScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
    }
    override fun initializeControls() {
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        tvScreenTitle.setText(R.string.active_delivery)
        val phoneNumber  = preferenceUtils.getStringFromPreference(PreferenceUtils.PHONE, "")
        val dateTime     = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT_DT, "")
        val name         = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
        val email        = preferenceUtils.getStringFromPreference(PreferenceUtils.EMAIL, "")

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ActiveDeliveryActivity)

        val tvName        = findViewById(R.id.tvName) as TextView
        val tvEmail       = findViewById(R.id.tvEmail) as TextView
        val tvPhoneNumber = findViewById(R.id.tvPhoneNumber) as TextView
        val tvDateTime    = findViewById(R.id.tvDateTime) as TextView

        tvName.setText(name)
        tvPhoneNumber.setText(phoneNumber)
        tvEmail.setText(email)
        tvDateTime.setText(dateTime)

        val btnStartRoute = findViewById(R.id.btnStartRoute) as Button
        btnStartRoute.setOnClickListener {
            val intent = Intent(this@ActiveDeliveryActivity, StartRouteActivity::class.java);
            startActivity(intent);
        }

        val btnCancelRoute = findViewById(R.id.btnCancelRoute) as Button
        btnCancelRoute.setOnClickListener {
            val intent = Intent(this@ActiveDeliveryActivity, CancelRouteActivity::class.java);
            startActivity(intent);
        }

        val btnPod = findViewById(R.id.btnPod) as Button
        btnPod.setOnClickListener {
            val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java);

            startActivity(intent);
        }

        val btnShipment = findViewById(R.id.btnShipment) as Button
        btnShipment.setOnClickListener {
            val intent = Intent(this@ActiveDeliveryActivity, ShipmentDetailsActivity::class.java);
            startActivity(intent);
        }

        val btnEvent = findViewById(R.id.btnEvent) as Button
        btnEvent.setOnClickListener {
            val intent = Intent(this@ActiveDeliveryActivity, RegisterEventsActivity::class.java);
            startActivity(intent);
        }
    }
}